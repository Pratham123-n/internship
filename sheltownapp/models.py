from django.conf import settings
from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator,MaxValueValidator, MinLengthValidator , RegexValidator
from django.template.defaultfilters import slugify
from django.db.models import Avg
from django.urls import reverse
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.db import IntegrityError
from PIL import Image
from taggit.managers import TaggableManager
from django.db.models.signals import post_save
from django.dispatch import receiver
import uuid
import requests
import random 
# Create your models here.
class User(models.Model):

    USER_CATEGORIES = (
    ('Property Owner', 'Property Owner'),
    ('Service Provider', 'Service Provider'),
    ('User', 'User'),
    )

    f_name       = models.CharField(max_length=100, default="",blank=True)    
    l_name       = models.CharField(max_length=100, default="",blank=True)
    email        = models.EmailField(blank=True)
    user         = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, related_name='custom_user')
    dob          = models.DateField(blank = True,null=True)
    phone_regex = RegexValidator(regex=r'^[6-9]{1}[0-9]{9}$', message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9")
    phone_number = models.CharField(validators=[phone_regex], max_length=10, blank=True) # phone number and email and user type are required blank = True for saving via post_save signal of default user
    address_1    = models.CharField(max_length=250, default="",blank = True)
    address_2    = models.CharField(max_length=250, default="",blank = True)
    city         = models.CharField(max_length=100, default="",blank = True)
    state        = models.CharField(max_length=100, default="",blank = True)
    zip_code     = models.CharField(max_length=20 , default="",blank = True)
    is_sheltown_verified = models.BooleanField(default=False)
    user_image   = models.ImageField(upload_to='user_profile_image/',null=True,blank=True, default="default_user_image.jpg")
    usertype    = models.CharField(max_length=30,validators=[MinLengthValidator(1)] ,choices=USER_CATEGORIES, blank=True)
    identity_proof = models.FileField(upload_to='identity_proof/',blank=True)

    def __str__(self):
        return self.user.username
        # "{} {}".format(self.f_name, self.l_name)
    #ADHAAR TO BE INCLUDED IN SUBCLASS WITH MANDATORY FOR TENANTS
    # def save(self, *args, **kwargs):
    #     super(User, self).save(*args, **kwargs)

    #     img = Image.open(self.user_image.path)

    #     if img.height > 300 or img.width > 300:
    #         output_size = (150, 150)
    #         img.thumbnail(output_size)
    #         img.save(self.user_image.path)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        User.objects.create(user=instance)
    instance.custom_user.save()



class Landingpage_upperimg(models.Model): 
    upper_img = models.ImageField() 

class Landingpage_lowerimg(models.Model): 
    lower_mobile_img = models.ImageField(null=True)
    lower_tab_img = models.ImageField(null=True)
    lower_desktop_img = models.ImageField(null=True)  

class Locality(models.Model):
    city     = models.CharField(max_length=100)
    state    = models.CharField(max_length=100)
    locality = models.CharField(max_length=150)

    def __str__(self):
        return "{}, {}, {}".format(self.locality, self.city, self.state)

    class Meta:
        unique_together     = ('city', 'state','locality')
        verbose_name_plural = 'localities'
        
class Feedback(models.Model):
    one_to_five_choices     = list(zip(range(1,5+1), range(1,5+1)))
    
    user                    = models.ForeignKey(User, on_delete=models.CASCADE, related_name='feedbacks')
    is_testimonial_verified = models.BooleanField(default=False)
    is_landingpage_verified = models.BooleanField(default=False)
    content                 = models.TextField() #https://stackoverflow.com/questions/37133336/new-line-on-django-admin-text-field?rq=1
    dob                     = models.DateField(auto_now=True)
    rating                  = models.PositiveSmallIntegerField(choices=one_to_five_choices)

class Coupon(models.Model):
        code =models.CharField(max_length=50,unique=True)
        valid_from = models.DateTimeField()
        valid_to = models.DateTimeField()
        discount = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(100)])#discount percentage
        active =models.BooleanField()
        locality = models.ForeignKey(Locality,null=True, blank=False, on_delete=models.SET_NULL)#To be verified against city/State
        service_type = models.CharField(max_length=100,choices=(("Personalized","Personalized"), ("Rental","Rental")))
        min_amount = models.IntegerField(default=0)#zero if unspecified
        display_text =models.TextField()
        to_be_displayed_in_rental_details_page = models.BooleanField(default=False)
        to_be_displayed_in_personalised_page = models.BooleanField(default=False)
        image_DESKTOPVIEW_with_all_information_to_be_displayed = models.ImageField(default= None, null=True)
        image_MOBILEVIEW_with_all_information_to_be_displayed = models.ImageField(default= None, null=True)
        image_TABVIEW_with_all_information_to_be_displayed = models.ImageField(default= None, null=True)

        def __str__(self):
            return self.code

class PropertyOwner(models.Model):

    ROLE = (('Owner','Owner'),('Manager','Manager'))
    phone_regex = RegexValidator(regex=r'^[6-9]{1}[0-9]{9}$', message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9.")
    whatsapp_number = models.CharField(validators=[phone_regex], max_length=10, blank=False)
    prop_owner_designation    = models.CharField(max_length=30,validators=[MinLengthValidator(1)] ,choices=ROLE)
    cancelled_cheque  = models.ImageField(upload_to='prop_owner_cancel_cheque/')
    bank_account_number = models.CharField(max_length=250,blank=True)
    profile_user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,related_name='property_owner')

    def __str__(self):
        return "{} {}".format(self.profile_user.user.username,self.profile_user.phone_number)

class Amenity(models.Model):
    ambulance   = models.BooleanField(default=False)
    wifi = models.BooleanField(default=False)
    TV          = models.BooleanField(default=False)
    medical_centre = models.BooleanField(default=False)
    table_chair = models.BooleanField(default=False)
    guard = models.BooleanField(default=False)
    bed         = models.BooleanField(default=False)
    parking = models.BooleanField(default=False)
    laundry_service = models.BooleanField(default=False)
    microwave = models.BooleanField(default=False)
    balcony     = models.BooleanField(default=False)
    attached_bathroom = models.BooleanField(default=False)
    gym = models.BooleanField(default=False)
    CCTV = models.BooleanField(default=False)
    pool = models.BooleanField(default=False)
    lift =  models.BooleanField(default=False)
    AC          = models.BooleanField(default=False)
    cooler      = models.BooleanField(default=False)
    fridge = models.BooleanField(default=False)
    RO   = models.BooleanField(default=False)
    powerbackup = models.BooleanField(default=False)
    
    
    # gated = models.BooleanField(default=False)
    # biometric = models.BooleanField(default=False)
    # washing_machine = models.BooleanField(default=False)
    # coffee_machine = models.BooleanField(default=False)
    
    
    
    class Meta:
        verbose_name_plural = 'Amenities'    

class AdvancedAmenity(models.Model):
    amenity_name = models.CharField(max_length=100)
    amenity_icon = models.ImageField(upload_to='advanced_amenities_icon/')

    def __str__(self):
        return self.amenity_name
    class Meta:
        verbose_name_plural = 'AdvancedAmenities'
        
class CommonAreaRental(models.Model):
    name = models.CharField(max_length=50)
  

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'CommonAreasRental'

# class GenderPreference(models.Model):
#     gender_restrictions = models.CharField(max_length=50, choices =(('Family','Family'),('Female only','Female only'),('Male only','Male only'),('Sheltown+','Sheltown')))
#     description         = models.TextField(default = '', max_length=500, null=True, blank=True)

#     def __str__(self):
#         return self.get_gender_restrictions_display()

class PropertyManager(models.Manager):
    def set_rating(self, properties):
        """
        Take a list of property, and set their rating according to reviews.
        """
        for property in properties:
            property.set_rating()

class Property(models.Model):

    SECURITY_DEPOSIT_CHOICES = (
    ('None', 'No Security Deposit'),
    ('1 month', '1 month Security Deposit'),
    ('2 month', '2 month Security Deposit'),
    ('3 month', '3 month Security Deposit'),
    ('4 month', '4 month Security Deposit'),
    )

    PROPERTY_CATEGORIES = (
    ('flat/apartment 1BHK','flat/apartment 1BHK'),
    ('flat/apartment 2BHK','flat/apartment 2BHK'),
    ('flat/apartment 3BHK','flat/apartment 3BHK'),
    ('house','house'),
    ('pg/hostel +1 mate','pg/hostel +1 mate'),
    ('pg/hostel +2 mate','pg/hostel +2 mate'),
    ('pg/hostel +3 mate','pg/hostel +3 mate'),
    ('pg/hostel +4 mate','pg/hostel +4 mate'),    
    )

    name        = models.CharField(max_length=200)
    status      = models.CharField(max_length=50,blank=False, choices=(('Not Avaliable','Not Avaliable'),('Avaliable now','Avaliable now')))
    proptype    = models.CharField(max_length=100, choices=PROPERTY_CATEGORIES)
    total_no_of_beds  = models.PositiveSmallIntegerField()
    gender_restrictions = models.CharField(max_length=50, choices =(('Family+','Family+'),('Female only','Female only'),('Male only','Male only'),('Sheltown+','Sheltown+')))
    owner_stays = models.BooleanField()
    max_bed_in_room = models.PositiveSmallIntegerField()
    non_veg_allowed = models.BooleanField()
    housekeeping_freq = models.CharField(max_length=50, choices=(('Weekly','Weekly'),('Daily','Daily'),('None','None')))
    visitor_allowed = models.BooleanField()
    rating = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(5)], max_digits=3, decimal_places=1, default = 2.5)
    amenities = models.ForeignKey(Amenity,null=True, blank=True, on_delete=models.SET_NULL)
    advanced_amenities = models.ManyToManyField(AdvancedAmenity, blank=True,related_name='advanced_amenities' )
    security_deposit = models.CharField(max_length=20 , choices=SECURITY_DEPOSIT_CHOICES, default='None')
    furnished_type = models.CharField(max_length=20, default='Semi Furnished',choices=(('Fully Furnished','Fully Furnished'),('Semi Furnished','Semi Furnished')))
    rent = models.DecimalField(validators=[MinValueValidator(0)], max_digits=12, decimal_places=2,default=0.00)
    locality = models.ForeignKey(Locality,null=True, blank=False, on_delete=models.SET_NULL)
    address_1    = models.CharField(max_length=250, default="")
    address_2    = models.CharField(max_length=250, default="")
    # nearby       = models.TextField(default = '', max_length=500, null=True, blank=True)
    virtual_tour = models.CharField(default='',max_length=400,blank=True)
    # nearby = models.OneToManyField(Landmark, blank=True,)
    percentage_discount_from0to100 = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(100)],max_digits=5, decimal_places=2, default = 0.00)
    objects = PropertyManager()
    description_by_property_owner = models.TextField(default=None , null=True )
    is_sheltown_verified = models.BooleanField(default=False)
    is_meals_included = models.BooleanField(default=False)
    electricity_included = models.BooleanField(default=False)
    smoking_drinking_allowed = models.BooleanField(default=False)
    common_areas = models.ManyToManyField(CommonAreaRental, blank=True,related_name='common_areas' )
    property_owner = models.ForeignKey(PropertyOwner, blank=False, on_delete=models.SET_NULL, null=True, related_name='properties')

    def __str__(self):
            return self.name

    class Meta:
        verbose_name_plural = 'Properties'

    def set_rating(self):
        reviews = PropertyReview.objects.filter(property=self)
        if reviews:
            self.rating = reviews.aggregate(Avg('rating'))['rating__avg']
        else:
            self.rating = 2.5
        
        self.save()  
	
    def get_price_diff(self):
        return int((self.percentage_discount_from0to100 * self.rent)/ 100)   

    
    def get_discounted_price(self):
        return ((100 - self.percentage_discount_from0to100) * self.rent )/ 100   

	#def get_price_diff(self):
	# 	return int((self.percentage_discount_from0to100 * self.rent)/ 100)

    def get_absolute_url(self):
        return reverse('property-detail', kwargs={'pk':self.pk})



def get_image_filename(instance, filename):
        name = instance.property.name
        slug = slugify(name)
        return "property_images/%s-%s" % (slug, filename)

class Landmark(models.Model):
    property = models.ForeignKey(Property, default=None,on_delete=models.CASCADE, related_name='nearby') 
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

# Property Images
class PropertyImages(models.Model):
    property = models.ForeignKey(Property, default=None,on_delete=models.CASCADE,related_name='images')
    image    = models.ImageField(upload_to=get_image_filename,
                              verbose_name='PropertyImage')
    def __str__(self):
            return self.property.name

    class Meta:
        verbose_name_plural = 'Property Images'

class Query(models.Model):
    name            = models.CharField(max_length = 100, default = '')
    email           = models.EmailField()
    Number    = models.CharField(max_length = 10, blank = True)
    message         = models.TextField(max_length = 1000, blank = False, default = '')

    class Meta:
        verbose_name_plural ='Queries'

class PropertyReview(models.Model):
    property = models.ForeignKey(Property,related_name='reviews', default=None,on_delete=models.CASCADE)
    review = models.TextField(validators=[MinLengthValidator(1)])
    user   = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviews')
    rating = models.PositiveSmallIntegerField( choices=( (1,1),(2,2),(3,3),(4,4),(5,5) ) )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
            return "{}  By:- {}{}".format(self.property.name, self.user.f_name,self.user.l_name)
    
    def get_total_likes(self):
        return self.likes.users.count()

    def get_total_dis_likes(self):
        return self.dis_likes.users.count()
    
    class Meta:
        verbose_name_plural = 'PropertyReviews'

class PropertyReviewLike(models.Model):
    ''' like  property review '''

    review = models.OneToOneField(PropertyReview, related_name="likes", on_delete=models.CASCADE)
    users = models.ManyToManyField(User, related_name='property_review_likes')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.review.property.name)

class PropertyReviewDisLike(models.Model):
    ''' Dislike  property review '''

    review = models.OneToOneField(PropertyReview, related_name="dis_likes", on_delete=models.CASCADE)
    users = models.ManyToManyField(User, related_name='property_review_dis_likes')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.review.property.name)





class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ['title']

    def __str__(self):
        return self.title



class BlogPost(models.Model):
    # author          = models.ForeignKey(User, default = None, on_delete = models.CASCADE, null =True, blank = True)
    title           = models.CharField(max_length=200)
    # created_date    = models.DateTimeField(default = timezone.now())
    published_date  = models.DateTimeField(blank = True, null =True)
    tags            = TaggableManager()
    slug            = models.SlugField(max_length=200,blank=True)
    body            = models.TextField()
    image           = models.ImageField()
    category = models.ForeignKey(Category, verbose_name="Category",on_delete = models.CASCADE,default = None,null =True, blank = True)
    likes           =models.ManyToManyField(User,related_name='blog_posts',blank=True)  
    min_to_read     = models.IntegerField(default=5)
    dislikes           =models.ManyToManyField(User,related_name='blog_post',blank=True)  

    def total_likes(self):
        return self.likes.count()

    def total_dislikes(self):
        return self.likes.count()
        
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def approve_comments(self):
        return self.comments.filter(approved_comment = True)

    def get_absolute_url(self):
        return reverse("blog-details", kwargs = {'id': self.id})

    def add_comment_absolute_url(self):
        return reverse("add-comment", kwargs = {'id': self.id})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'BlogPosts'
    def send(self, request):
        title = self.title
        subscribers = Subscriber.objects.filter(confirmed=True)
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        for sub in subscribers:
            message = Mail(
                    from_email=settings.FROM_EMAIL,
                    to_emails=sub.email,
                    subject='New Post Added',
                    html_content=title + (
                        '<br><a href="{}/delete/?email={}&conf_num={}">Unsubscribe</a>.').format(
                            request.build_absolute_uri('/delete/'),
                            sub.email,
                            sub.conf_num))
            sg.send(message)

class Comment(models.Model):
    post             = models.ForeignKey(BlogPost, default = None, on_delete = models.CASCADE,related_name="comments")
    public_name      = models.CharField(max_length = 100, null =True)
    text             = models.TextField(max_length=2000)
    created_date     = models.DateTimeField(auto_now_add=True)
    approved_comment = models.BooleanField(default = False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def get_absolute_url(self):
        return reverse('post_list')  

    def __str__(self):
        return self.text      

    class Meta:
        verbose_name_plural = 'Comments'

class How_sheltown_is_helping_us_text_in_rental_details(models.Model):
    list_item_text = models.TextField()

    def __str__(self):
        return self.list_item_text       

class Subscriber(models.Model):
    email = models.EmailField()
    conf_num = models.CharField(max_length=15)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.email + " (" + ("not " if not self.confirmed else "") + "confirmed)"



class PersonalServiceCategory(models.Model):
    name = models.CharField(max_length=400,unique=True) 

    def __str__(self):
        return self.name
    class Meta:
        unique_together = ('name',)
        verbose_name_plural = ' PersonalServiceCategories'

class PersonalService(models.Model):
    name = models.CharField(max_length=400,unique=True)
    category = models.ForeignKey(PersonalServiceCategory,on_delete=models.CASCADE , related_name='services')
    image = models.ImageField()

    def __str__(self):
        return self.name
    class Meta:
        unique_together     = ('name',)        

class PersonalServiceSubcategory1(models.Model):
    name = models.CharField(max_length=400) 
    main_service = models.ForeignKey(PersonalService,on_delete=models.CASCADE,related_name='subcat1')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'PersonalServiceSubcategory1'

class PersonalServiceSubcategory2(models.Model):
    name = models.CharField(max_length=400)
    service = models.ForeignKey(PersonalServiceSubcategory1,on_delete=models.CASCADE,related_name='subcat2' )

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'PersonalServiceSubcategory2'

class PersonalServiceIssues(models.Model):
    name = models.CharField(max_length=400) 
    price = models.DecimalField(validators=[MinValueValidator(0)], max_digits=12, decimal_places=2,default=0.00)
    main_service = models.ForeignKey(PersonalService,on_delete=models.SET_NULL, null=True,blank=True, related_name='of_service' )
    subcategory1 = models.ForeignKey(PersonalServiceSubcategory1,on_delete=models.SET_NULL, null=True ,blank=True, related_name='of_subcat1')
    subcategory2 = models.ForeignKey(PersonalServiceSubcategory2,on_delete=models.SET_NULL, null=True ,blank=True, related_name='of_subcat2')
    locality = models.ForeignKey(Locality,null=True, blank=False, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'PersonalServiceIssues'        

class PersonalServiceReview(models.Model):
    personalservice = models.ForeignKey(PersonalService,related_name='reviews', default=None,on_delete=models.CASCADE)
    review = models.TextField(validators=[MinLengthValidator(1)])
    user   = models.ForeignKey(User, on_delete=models.CASCADE, related_name='personalreviews')
    rating = models.PositiveSmallIntegerField( choices=( (1,1),(2,2),(3,3),(4,4),(5,5) ) )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
            return "{}  By:- {}{}".format(self.personalservice.name, self.user.f_name,self.user.l_name)
    
    def get_total_likes(self):
        return self.likes.users.count()

    def get_total_dis_likes(self):
        return self.dis_likes.users.count()
    
    class Meta:
        verbose_name_plural = 'PersonalServiceReviews'

class PersonalServiceReviewLike(models.Model):
    ''' like  property review '''

    review = models.OneToOneField(PersonalServiceReview, related_name="likes", on_delete=models.CASCADE)
    users = models.ManyToManyField(User, related_name='personal_review_likes')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.review.personalservice.name)

class PersonalServiceReviewDisLike(models.Model):
    ''' Dislike  property review '''

    review = models.OneToOneField(PersonalServiceReview, related_name="dis_likes", on_delete=models.CASCADE)
    users  = models.ManyToManyField(User, related_name='personal_review_dis_likes')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.review.personalservice.name)    

class PersonalCallbackRequest(models.Model):
    phone_regex = RegexValidator(regex=r'^[6-9]{1}[0-9]{9}$', message="Phone number must be entered in the format: '999999999'. Starting digit can be 6,7,8,9.")
    phone_number = models.CharField(validators=[phone_regex], max_length=10, blank=False) # validators should be a list    
    category = models.ForeignKey(PersonalServiceCategory,on_delete=models.CASCADE , related_name='callbackrequest')

    def __str__(self):
        return "{} wants a callback regarding {} services".format(self.phone_number, self.category.name)  

    class Meta:
        unique_together     = ('phone_number', 'category')
        verbose_name_plural = 'PersonalCallbackRequest'


    
class ServiceProvider(models.Model):
    EXPERIENCE_CHOICES = (
    ('0-1 years', '0-1 years'),
    ('2-4 years', '2-4 years'),
    ('3-5 years', '3-5 years'),
    ('5-7 years', '5-7 years'),
    ('8-10 years', '8-10 years'),
    ('10+ years', '10+ years'),
    )
    phone_regex = RegexValidator(regex=r'^[6-9]{1}[0-9]{9}$', message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9.")
    whatsapp_number = models.CharField(validators=[phone_regex], max_length=10, blank=False)
    has_assistant    = models.BooleanField(default=False)
    cancelled_cheque  = models.ImageField(upload_to='prop_owner_cancel_cheque/')
    bank_account_number = models.CharField(max_length=250,blank=True)
    experience_in_years = models.CharField(max_length=30,validators=[MinLengthValidator(1)] ,choices=EXPERIENCE_CHOICES)
    services_offered = models.ManyToManyField(PersonalService, blank=True,related_name='services_offered')
    services_offered_in = models.ManyToManyField(Locality, blank=False,related_name='localities_served')
    course_details = models.TextField(blank=True,max_length=250)
    profile_user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,related_name='service_provider')
    is_available = models.BooleanField(default=True)


    def __str__(self):
        return "{} {}".format(self.profile_user.user.username,self.profile_user.phone_number)
class Policy(models.Model):
    policy_name=models.CharField(max_length=100,null=True)
    policy_text=models.TextField()

class Job_locality(models.Model):
    job_type = models.CharField(max_length=100,null=True)
    job_locality = models.CharField(max_length=100,null=True)

class Jobs(models.Model):
    job_email_content = models.CharField(max_length=100,null=True)
    # job_type = models.ForeignKey(Job_locality,on_delete = models.CASCADE,default = None,null =True, blank = True)
    job_location=models.CharField(max_length=400)
    job_type = models.CharField(max_length=100)
    description=models.TextField()
    experience=models.TextField()
    lastdate=models.DateTimeField()
    # time_period=models.TextField()
    
    def send(self, request):
        job = self.job_email_content
        subscribers = Jobsubscriber.objects.filter(confirmed=True)
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        for sub in subscribers:
            message = Mail(
                    from_email=settings.FROM_EMAIL,
                    to_emails=sub.email,
                    subject='New Job Added',
                    html_content=job + (
                        '<br><a href="{}/delete/?email={}&conf_num={}">Unsubscribe</a>.').format(
                            request.build_absolute_uri('/alertdelete/'),
                            sub.email,
                            sub.conf_num))
            sg.send(message)

class Jobsubscriber(models.Model):
    email = models.EmailField()
    conf_num = models.CharField(max_length=15)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.email + " (" + ("not " if not self.confirmed else "") + "confirmed)"

class jobapplicant(models.Model):
    resume =models.FileField(upload_to='uploads/')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='jobapplicant')
    job= models.ForeignKey(Jobs,on_delete=models.CASCADE, related_name='jobapplicant',null=True ,blank=True)
    # applied = models.BooleanField(default=False)

class Aboutus(models.Model):
    heading=models.CharField(max_length=250)
    text=models.TextField()
    points=models.TextField()
class Brokage(models.Model):
    brokage=models.CharField(max_length=250)
    description=models.TextField()

class Family(models.Model):
    name=models.CharField(max_length=15)
    designation=models.CharField(max_length=15)
    description=models.TextField()
    Image =models.ImageField()

class IntialTeam(models.Model):
    name=models.CharField(max_length=15)
    qualification=models.CharField(max_length=15)
    Image =models.ImageField()

class PersonalBooking(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    booking_user = models.ForeignKey(User, blank=False, on_delete=models.SET_NULL, null=True,related_name="appointments")
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(blank=True)
    address_1 = models.TextField()
    address_2 = models.TextField(blank=True)
    state = models.CharField(max_length=20)
    locality = models.ForeignKey(Locality,null=True, blank=False, on_delete=models.SET_NULL)
    zip_code = models.PositiveIntegerField()
    scheduled_datetime = models.DateTimeField()
    promo_code = models.CharField(max_length=50,blank=True)
    booking_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    total_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    post_service_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    first_payment = models.BooleanField(default=False)
    second_payment = models.BooleanField(default=False)
    unassigned = models.BooleanField(default=True)
    service_partner = models.ForeignKey(ServiceProvider, blank=True, on_delete=models.SET_NULL, null=True,related_name='bookings')
    personal_service = models.CharField(max_length=50,default="")
    personal_subcat1 = models.CharField(max_length=50,blank=True)
    personal_subcat2 = models.CharField(max_length=50,blank=True)
    personal_service_issue = models.TextField(default="")
    first_payment_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2,default=0)
    is_completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    second_payment_mode = models.CharField(max_length=20,choices=(('paytm','paytm'),('cash','cash')),default='paytm')

    def send(self, request):
        if self.service_partner is None:
            return
        otp=""
        for i in range(6):
            otp+=str(random.randint(1,9))
            
        cust_pn = self.booking_user.phone_number
        ser_pn = self.service_partner.profile_user.phone_number
        r = requests.post('http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS', data = {
            'From':'SHELTO',
            'To':cust_pn,
            'TemplateName':'booking',
            'VAR1':self.booking_user.user.username,
            'VAR2':self.personal_service,
            'VAR3':otp,
            'VAR4':'{} {}'.format(self.service_partner.profile_user.f_name,self.service_partner.profile_user.l_name),
            })
        r = requests.post('http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS', data = {
            'From':'SHELTO',
            'To':ser_pn,
            'TemplateName':'appointment',
            'VAR1':'{} {}'.format(self.service_partner.profile_user.f_name,self.service_partner.profile_user.l_name),
            'VAR2':self.personal_service,
            'VAR3':otp,
            })

# Hello #VAR1#,
# You have #VAR2# service appointment, #VAR3# can be verified with customer for security purposes. Please login in to www.sheltown.com
# for more information.
# Thanks Sheltown Team

# Hello #VAR1#, Your #VAR2# service booking is registered, #VAR3# can be verified with #VAR4# for security purposes.
# Thanks Sheltown Team
# /{api_key}/ADDON_SERVICES/SEND/TSMS
#  r = requests.post('https://httpbin.org/post', data = {'key':'value'})
 


class PersonalBookingTransaction(models.Model):
    TXNID  = models.CharField(max_length=50,default="")
    ORDERID = models.CharField(max_length=50,default="")
    BANKTXNID = models.CharField(max_length=50,default="")
    TXNAMOUNT = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    STATUS = models.CharField(max_length=50,default="")
    RESPCODE = models.CharField(max_length=50,default="")
    TXNDATE = models.CharField(max_length=50, default="")
    PAYMENTMODE = models.CharField(max_length=50, default="")
    transaction_json = models.TextField()
    booking =  models.ForeignKey(PersonalBooking, blank=True, on_delete=models.SET_NULL, null=True,related_name="transactions")

class GSTServiceTax(models.Model):
    gst_percent_rental = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(100)],max_digits=5, decimal_places=2,default=18.00)
    servicetax_percent_rental = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(100)],max_digits=5, decimal_places=2,default=5.00)
    gst_percent_personal = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(100)],max_digits=5, decimal_places=2,default=18.00)
    servicetax_percent_personal = models.DecimalField(validators=[MinValueValidator(0),MaxValueValidator(100)],max_digits=5, decimal_places=2,default=5.00)

class Query24_7_fromuser(models.Model):
    user  = models.ForeignKey(User, on_delete=models.CASCADE, related_name='query24_7')
    message = models.TextField() 
    created_at = models.DateTimeField(auto_now_add=True)
    response = models.TextField(default="")
    def __str__(self):
        return self.user.usertype

class RentalBooking(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    booking_user = models.ForeignKey(User, blank=False, on_delete=models.SET_NULL, null=True,related_name="rentalbookings")
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField(blank=True)
    checkin_date = models.DateField()
    checkin_time = models.TimeField()
    phone_regex = RegexValidator(regex=r'^[6-9]{1}[0-9]{9}$', message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9")
    phone_number = models.CharField(validators=[phone_regex], max_length=10, blank=True)
    pg_sharing = models.PositiveSmallIntegerField(choices=((1,1),(2,2),(3,3),(4,4)), null=True, blank=True)
    booked_property = models.ForeignKey(Property,blank=False, on_delete=models.SET_NULL, null=True)
    promo_code = models.CharField(max_length=50,blank=True)
    booking_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    total_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    post_booking_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    booking_with_tax_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    property_partner = models.ForeignKey(PropertyOwner, blank=False, on_delete=models.SET_NULL, null=True,related_name="bookings")
    first_payment = models.BooleanField(default=False)
    security_deposit =  models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2,default=0)
    move_out_date = models.DateField(blank=True,null=True)
    moved_out = models.BooleanField(default=False)
    is_confirmed = models.BooleanField(default=False)
    is_cancelled = models.BooleanField(default=False)

    def send(self, request):

        self.is_confirmed = True
        self.save()
        otp=""
        for i in range(6):
            otp+=str(random.randint(1,9))
            
        cust_pn = self.booking_user.phone_number
        ser_pn = self.property_partner.profile_user.phone_number
        data_1 = {
            'From':'SHELTO',
            'To':cust_pn,
            'TemplateName':'rentalbooking',
            'VAR1': self.booking_user.user.username,
            'VAR2': self.booked_property,
            'VAR3':self.booked_property.proptype,
            'VAR4':'{} {}'.format(self.checkin_time,self.checkin_date),
            'VAR5' : otp,
            }
        data_2 = {
            'From':'SHELTO',
            'To':ser_pn,
            'TemplateName':'rentalbooking',
            'VAR1':'{} {}'.format(self.property_partner.profile_user.f_name,self.property_partner.profile_user.l_name),
            'VAR2': self.booked_property,
            'VAR3':self.booked_property.proptype,
            'VAR4':'{} {}'.format(self.checkin_time,self.checkin_date),
            'VAR5' : otp,
            }
        r = requests.post('http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS', data_1)
        r = requests.post('http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS', data_2)

        
        
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        
        message = Mail(
                from_email=settings.FROM_EMAIL,
                to_emails=self.booking_user.email,
                subject='Rental Booking Confirmed',
                html_content='Hello {},<br>Your {} {} booking at {} is confirmed, {} is the otp which can be verified with property owner for security purposes.<br><br>Thanks and Regards<br>The Sheltown Team'.format(data_1['VAR1'],data_1['VAR2'],data_1['VAR3'],data_1['VAR4'],data_1['VAR5'])
                )

        sg.send(message)
        message = Mail(
                from_email=settings.FROM_EMAIL,
                to_emails=self.email,
                subject='Rental Booking Confirmed',
                html_content='Hello {},<br>Your {} {} booking at {} is confirmed, {} is the otp which can be verified with property owner for security purposes.<br><br>Thanks and Regards<br>The Sheltown Team'.format(data_1['VAR1'],data_1['VAR2'],data_1['VAR3'],data_1['VAR4'],data_1['VAR5'])
                )

        sg.send(message)        

        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        
        message = Mail(
                from_email=settings.FROM_EMAIL,
                to_emails=self.property_partner.profile_user.email,
                subject='Rental Booking Recieved',
                html_content='Hello {},<br>You have received {} {} booking for {} , {} is the otp which can be verified with tenant for security purposes.<br><br>Thanks and Regards<br>The Sheltown Team'.format(data_2['VAR1'],data_2['VAR2'],data_2['VAR3'],data_2['VAR4'],data_2['VAR5'])
                )

        sg.send(message)


# proppartnerbooking
# 	Hello #VAR1#,
# You have received #VAR2# #VAR3# booking for #VAR4#, #VAR5# can be verified with tenant for security purposes. Please login in to www.sheltown.com
# for more information.

# Thanks Sheltown Team

# rentalbooking
# 	Hello #VAR1#,
# Your #VAR2# #VAR3# booking at #VAR4# is confirmed, #VAR5# can be verified with property owner for security purposes.

# Thanks Sheltown Team

class RentalRecurrentBooking(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    booking_user = models.ForeignKey(User, blank=False, on_delete=models.SET_NULL, null=True, related_name="rentalrecbookings")
    for_start_date = models.DateField()
    for_end_date = models.DateField()
    booked_property = models.ForeignKey(Property,blank=False, on_delete=models.SET_NULL, null=True)
    promo_code = models.CharField(max_length=50,blank=True)
    second_payment = models.BooleanField(default=False)
    second_belongs_to = models.ForeignKey(RentalBooking, blank=True, on_delete=models.SET_NULL, null=True)
    property_partner = models.ForeignKey(PropertyOwner, blank=False, on_delete=models.SET_NULL, null=True, related_name="rec_bookings")
    total_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    payment_amount = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    due_date = models.DateField(blank=True)
    payment_complete = models.BooleanField(default=False)
    moved_out = models.BooleanField(default=False)
    next_month_payment_generated = models.BooleanField(default=False)
    
class RentalBookingTransaction(models.Model):
    TXNID  = models.CharField(max_length=50,default="")
    ORDERID = models.CharField(max_length=50,default="")
    BANKTXNID = models.CharField(max_length=50,default="")
    TXNAMOUNT = models.DecimalField(validators=[MinValueValidator(0)],max_digits=10, decimal_places=2)
    STATUS = models.CharField(max_length=50,default="")
    RESPCODE = models.CharField(max_length=50,default="")
    TXNDATE = models.CharField(max_length=50, default="")
    PAYMENTMODE = models.CharField(max_length=50, default="")
    transaction_json = models.TextField(blank=True)
    booking =  models.ForeignKey(RentalBooking, blank=True, on_delete=models.SET_NULL, null=True,related_name="transactions")
    booking_recurrent = models.ForeignKey(RentalRecurrentBooking, blank=True, on_delete=models.SET_NULL, null=True,related_name="transactions")

class HomePageThreeImages(models.Model):
    personalservice   = models.ImageField( default="ps.png")
    rentalservice   = models.ImageField( default="rs.png")
    whysheltown   = models.ImageField( default="portfolio_why.png")
    promise_text  = models.TextField()

class BecomePartner(models.Model):
    rooms = models.IntegerField()
    beds = models.IntegerField()
    citiesupper = models.IntegerField()
    citieslower = models.IntegerField()
    partners = models.IntegerField()
    services = models.IntegerField()
    proppartner = models.ImageField()
    servpartner = models.ImageField()


    

