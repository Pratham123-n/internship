# Generated by Django 3.0.7 on 2020-07-01 00:56

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0050_auto_20200701_0011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpost',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 1, 0, 56, 53, 68647, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 1, 0, 56, 53, 72627, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='property',
            name='advanced_amenities',
            field=models.ManyToManyField(blank=True, related_name='advanced_amenities', to='sheltownapp.AdvancedAmenity'),
        ),
    ]
