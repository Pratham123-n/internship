# Generated by Django 3.0.7 on 2020-07-24 01:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0095_personalserviceissues_locality'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='coupon',
            name='location',
        ),
        migrations.AddField(
            model_name='coupon',
            name='locality',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='sheltownapp.Locality'),
        ),
    ]
