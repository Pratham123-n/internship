# Generated by Django 3.0.7 on 2020-07-12 01:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0076_auto_20200712_0126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
