# Generated by Django 3.0.7 on 2020-07-10 17:05

import datetime
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0069_auto_20200709_1905'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonalServiceReview',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('review', models.TextField(validators=[django.core.validators.MinLengthValidator(1)])),
                ('rating', models.PositiveSmallIntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name_plural': 'PersonalServiceReviews',
            },
        ),
        migrations.RemoveField(
            model_name='coupon',
            name='image_with_all_information_to_be_displayed',
        ),
        migrations.AddField(
            model_name='coupon',
            name='image_DESKTOPVIEW_with_all_information_to_be_displayed',
            field=models.ImageField(default=None, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='coupon',
            name='image_MOBILEVIEW_with_all_information_to_be_displayed',
            field=models.ImageField(default=None, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='coupon',
            name='image_TABVIEW_with_all_information_to_be_displayed',
            field=models.ImageField(default=None, null=True, upload_to=''),
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 10, 17, 5, 42, 217112, tzinfo=utc)),
        ),
        migrations.AlterUniqueTogether(
            name='personalservice',
            unique_together={('name',)},
        ),
        migrations.CreateModel(
            name='PersonalServiceReviewLike',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('review', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='likes', to='sheltownapp.PersonalServiceReview')),
                ('users', models.ManyToManyField(related_name='personal_review_likes', to='sheltownapp.User')),
            ],
        ),
        migrations.CreateModel(
            name='PersonalServiceReviewDisLike',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('review', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='dis_likes', to='sheltownapp.PersonalServiceReview')),
                ('users', models.ManyToManyField(related_name='personal_review_dis_likes', to='sheltownapp.User')),
            ],
        ),
        migrations.AddField(
            model_name='personalservicereview',
            name='personalservice',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='sheltownapp.PersonalService'),
        ),
        migrations.AddField(
            model_name='personalservicereview',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='personalreviews', to='sheltownapp.User'),
        ),
    ]
