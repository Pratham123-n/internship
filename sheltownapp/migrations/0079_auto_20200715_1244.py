# Generated by Django 3.0.7 on 2020-07-15 07:14

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0078_auto_20200713_1640'),
    ]

    operations = [
        migrations.CreateModel(
            name='Jobs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job', models.CharField(max_length=100, null=True)),
                ('Avaliable_in', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('experience', models.TextField()),
                ('date', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 15, 7, 14, 8, 526927, tzinfo=utc)),
        ),
    ]
