# Generated by Django 3.0.7 on 2020-07-15 20:06

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0082_auto_20200715_1947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 15, 20, 6, 44, 486205, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='jobs',
            name='lastdate',
            field=models.DateTimeField(default=datetime.datetime(2020, 7, 15, 20, 6, 44, 559477, tzinfo=utc)),
        ),
    ]
