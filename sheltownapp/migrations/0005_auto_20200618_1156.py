# Generated by Django 3.0.7 on 2020-06-18 11:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0004_property_virtual_tour'),
    ]

    operations = [
        migrations.CreateModel(
            name='Landmark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('landmark_name', models.CharField(max_length=150)),
            ],
        ),
        migrations.RemoveField(
            model_name='property',
            name='nearby',
        ),
        migrations.AddField(
            model_name='property',
            name='nearby',
            field=models.ManyToManyField(blank=True, to='sheltownapp.Landmark'),
        ),
    ]
