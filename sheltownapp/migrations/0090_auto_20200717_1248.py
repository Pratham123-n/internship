# Generated by Django 3.0.7 on 2020-07-17 07:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0089_auto_20200717_1246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobs',
            name='lastdate',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
