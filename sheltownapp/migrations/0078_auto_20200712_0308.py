# Generated by Django 3.0.7 on 2020-07-12 03:08

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sheltownapp', '0077_auto_20200712_0127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personalcallbackrequest',
            name='phone_number',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '999999999'. Starting digit can be 6,7,8,9.", regex='^[6-9]{1}[0-9]{9}$')]),
        ),
        migrations.AlterField(
            model_name='propertyowner',
            name='whatsapp_number',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9.", regex='^[6-9]{1}[0-9]{9}$')]),
        ),
        migrations.AlterField(
            model_name='serviceprovider',
            name='whatsapp_number',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9.", regex='^[6-9]{1}[0-9]{9}$')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone_number',
            field=models.CharField(blank=True, max_length=10, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '999999999'. Exactly 10 digits allowed.Starting digit can be 6,7,8,9", regex='^[6-9]{1}[0-9]{9}$')]),
        ),
    ]
