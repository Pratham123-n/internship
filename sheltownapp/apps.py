from django.apps import AppConfig


class SheltownappConfig(AppConfig):
    name = 'sheltownapp'
