import django_filters
from .models import Property
from django.db import models
from django import forms

class PropertyFilter(django_filters.FilterSet):
    price__gte = django_filters.NumberFilter(field_name='rent', lookup_expr='gte')
    price__lte = django_filters.NumberFilter(field_name='rent', lookup_expr='lte')
    PROPERTY_CATEGORIES = (
    ('flat/apartment 1BHK','1BHK Flat'),
    ('flat/apartment 2BHK','2BHK Flat'),
    ('flat/apartment 3BHK','3BHK Flat'),
    ('house','House'),
    ('pg/hostel +1 mate','PG with 1 mate'),
    ('pg/hostel +2 mate','PG with 2 mate'),
    ('pg/hostel +3 mate','PG with 3 mate'),
    ('pg/hostel +4 mate','PG with 4 mate'),    
    )
    SECURITY_DEPOSIT_CHOICES = (
    ('None', 'No Security Deposit'),
    ('1 month', '1 month Security Deposit'),
    ('2 month', '2 month Security Deposit'),
    ('3 month', '3 month Security Deposit'),
    ('4 month', '4 month Security Deposit'),
    )
    # proptype = django_filters.MultipleChoiceFilter(choices=PROPERTY_CATEGORIES,
    # widget=forms.CheckboxSelectMultiple)
    proptype = django_filters.ChoiceFilter(choices=PROPERTY_CATEGORIES)
    security_deposit = django_filters.ChoiceFilter(choices=SECURITY_DEPOSIT_CHOICES)
    furnished_type = django_filters.ChoiceFilter(choices=(('Fully Furnished','Fully Furnished'),('Semi Furnished','Semi Furnished')))
    gender_restrictions = django_filters.ChoiceFilter(choices=(('Family+','Family+'),('Female only','Female only'),('Male only','Male only'),('Sheltown+','Sheltown+')))
    class Meta:
        model = Property
        fields = ['proptype','security_deposit','furnished_type','gender_restrictions','rent']
        filter_overrides = {
            # models.CharField: {
            #     'filter_class': django_filters.CharFilter,
            #     'extra': lambda f: {
            #         'lookup_expr': 'icontains',
            #     },
            # },
            # models.BooleanField: {
            #     'filter_class': django_filters.BooleanFilter,
            #     'extra': lambda f: {
            #         'widget': forms.CheckboxInput,
            #     },
            # },
        }