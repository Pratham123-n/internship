from django import forms

from .models import Property,PropertyImages, Query, BlogPost, Comment,Locality, Landmark, PersonalCallbackRequest, PropertyOwner, ServiceProvider,PersonalBooking,RentalBooking,Amenity,Feedback
from .models import User as CustomUser
from .models import ServiceProvider,PropertyOwner, Locality,jobapplicant, PersonalServiceCategory, PersonalService
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User 
from django.db import transaction
from django.contrib.auth import password_validation
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.admin import widgets   
class PropertyForm(forms.ModelForm):
    # name  = forms.CharField(max_length=128)
    # label = forms.CharField(max_length=245, label="Item Description.")
    class Meta:
        model = Property
        fields = ('name', 'status', 'proptype', 'total_no_of_beds',
    'gender_restrictions' , 'owner_stays', 'max_bed_in_room' , 'non_veg_allowed',
    'housekeeping_freq' , 'visitor_allowed' , 'amenities',
    'security_deposit','furnished_type','rent','address_1','address_2','virtual_tour','description_by_property_owner','is_meals_included','smoking_drinking_allowed')

class AmenityForm(forms.ModelForm):
        class Meta:
            model = Amenity
            fields = '__all__'

            
        


class PropertyImageForm(forms.ModelForm):
    image = forms.ImageField(label='Image')    
    class Meta:
        model = PropertyImages
        fields = ('image', )


class LocalityForm(forms.ModelForm):
    city     = forms.CharField(max_length=100)
    state    = forms.CharField(max_length=100)
    locality = forms.CharField(max_length=150)  
    class Meta:
        model = Locality 
        fields = '__all__'

class NearbyForm(forms.ModelForm):  
    class Meta:
        model = Landmark 
        fields = ('name',)




class MessageForm(forms.ModelForm):
	class Meta:
		model = Query
		fields = '__all__'
		widgets = {
			'message' : forms.TextInput(attrs={'class':'messageinputclass'}),
            'Mobilenumber': forms.TextInput(attrs={'class':'noscroll',
            'placeholder':'+91',
            'pattern': "[2-9]{2}[0-9]{8}",
            'minlength':"10",
            'title' : "Please enter a valid phone number, of exactly 10 digits allowed and starting two digits can't be 0 or 1.",}),
         
		} 

# class BlogForm(forms.ModelForm):

#     class Meta:
#         model = BlogPost
#         fields = ('title', 'body')


#         widgets = {
#             'title': forms.TextInput(attrs={'class': 'textinputclass'}),
#             'body': forms.Textarea(attrs={'class': 'editable medium-editor-textarea postcontent'})
#            }

class CommentForm(forms.ModelForm):
    
    class Meta:
        model = Comment
        fields = ('public_name', 'text')  

        widgets = {
            'public_name': forms.TextInput(attrs={'class':'textinputclass'}),
            'text': forms.Textarea(attrs={'class': 'editable medium-editor-textarea'}) 
        }
class SubscriberForm(forms.Form):
    email = forms.EmailField(label='Your email',
                             max_length=100,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))
    def clean_email(self):
        # Get the email
        email = self.cleaned_data.get('email')

        # Check to see if any users already exist with this email as a username.
        try:
            match =Subscriber.objects.get(email=email)
        except Subscriber.DoesNotExist:
            # Unable to find a user, this is fine
            return email

        # A user was found with this as a username, raise an error.
        raise forms.ValidationError('This email address is already in use.')

class AlertForm(forms.Form):
    email = forms.EmailField(label='Your email',
                             max_length=100,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))

# class ResumeForm(forms.ModelForm):
#     class Meta:
#         model = jobapplicant
#         fields =  ('Resume',)



class PersonalCallbackRequestForm(forms.ModelForm):

    # message = forms.CharField(
    #     max_length=2000,
    #     widget=forms.Textarea(),
    #     help_text='Write here your message!'
    # )
    class Meta:
        model = PersonalCallbackRequest 
        fields =  ('phone_number',)
        # help_texts =  {
        #     'phone_number': 'Enter here',
        # }
        widgets = {
            'phone_number': forms.TextInput(attrs={'class':'noscroll',
            'placeholder':'+91',
            'pattern': "[6-9]{1}[0-9]{9}",
            'minlength':"10",
            'title' : "Please enter a valid phone number, of exactly 10 digits allowed and starting digit can be only 6,7,8,9.",
            }),
            
        }     
        labels = {
            'phone_number': _('Enter your mobile number'),
        }           
    # def clean(self):
    #     cleaned_data = super(ContactForm, self).clean()
    #     name = cleaned_data.get('name')
    #     email = cleaned_data.get('email')
    #     message = cleaned_data.get('message')
    #     if not name and not email and not message:
    #         raise forms.ValidationError('You have to write something!')

username_validator = UnicodeUsernameValidator()

class SignUpForm(UserCreationForm):

    email = forms.EmailField(help_text='Please enter a valid email address',
                            widget=forms.EmailInput(attrs={'class':'noscroll',
            'placeholder':'e-mail',          
            'style':"width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
            'title':'Enter a valid email-address',
            }))
    password1 = forms.CharField(label=_('Password'),
                                widget=(forms.PasswordInput(attrs={'placeholder':'Password',
            'title' : "Username and password would be used in login.",
            'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",})),
                                help_text=password_validation.password_validators_help_text_html())
    password2 = forms.CharField(label=_('Password Confirmation'), widget=forms.PasswordInput(attrs={ 'placeholder':'Confirm Password',
            'title':'Just Enter the same password, for confirmation',
            'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",}),
                                help_text=_('Just Enter the same password, for confirmation'))
    username = forms.CharField(
        label=_('Username'),
        max_length=150,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
       
        validators=[username_validator],
        error_messages={'unique': _("A user with that username already exists.")},
        widget=forms.TextInput(attrs={  'placeholder':'Username',
            'title' : "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.",
            'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
           'pattern': "[A-Za-z0-9@.+-_]{1,150}",})
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)
    
    def clean(self):
        # cleaned_data = super(SignUpForm, self).clean()
        # password = cleaned_data.get("password1")
        # confirm_password = cleaned_data.get("password2")

        # if password != confirm_password:
        #     raise forms.ValidationError(
        #         ""
        #     )        
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_(u'password and confirm password does not match, please try again'))

        return self.cleaned_data            

# class SignUpForm(UserCreationForm):
#     email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class':'noscroll',
#             'placeholder':'e-mail',          
#             'style':"width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
#             'title':'Enter a valid email-address',
#             }))
# #'password1', 'password2',
#     class Meta(UserCreationForm.Meta):
#         model = DefaultUser
#         fields = UserCreationForm.Meta.fields +  ('username', 'email',  ) 
#         widgets = {
#             'username': forms.TextInput(attrs={
#             'placeholder':'Username',
#             'title' : "Username and password would be used in login.",
#             'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
#             }),
#             'password1': forms.PasswordInput(attrs={
#             'placeholder':'Password',
#             'title' : "Username and password would be used in login.",
#             'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
#             }),
#             'password2': forms.PasswordInput(attrs={
#             'placeholder':'Confirm Password',
            
#             'style' : "width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
#             }),                
#         }
#         labels = {
#             'password1': _('Password'),
#             'password2': _('Confirm Password')
#         } 
    # @transaction.atomic
    # def save(self):
    #     user = super().save()
    #     custom_user = User.objects.create(user=user,
    #                                     email=self.cleaned_data.get('email'),
    #                                     phone_number='9999999999') #just some random phone value will be changed in view
    #     user.user.save()
    #     return user        

class PhoneVerificationForm(forms.Form):

    phone_number = forms.CharField(
        max_length=10,
        widget=forms.TextInput(attrs={
            'placeholder':'+91',
            'pattern': "[6-9]{1}[0-9]{9}",
            'minlength':"10",
            'title' : "Please enter a valid phone number, of exactly 10 digits allowed and and starting digit can be only 6,7,8,9.",
            'style' :"width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
            }),
        label='Enter your Phone Number'
    )
    
    user_type = forms.ChoiceField(choices=CustomUser.USER_CATEGORIES, widget=forms.RadioSelect(
        attrs={'class':'custom-user-type-radio-list',}
    ), label='Signup As')

    # def clean(self):
    #     cleaned_data = super(ContactForm, self).clean()
    #     name = cleaned_data.get('name')
    #     email = cleaned_data.get('email')
    #     message = cleaned_data.get('message')
    #     if not name and not email and not message:
    #         raise forms.ValidationError('You have to write something!')

class OTPVerificationForm(forms.Form):

    otp = forms.CharField(
        max_length=6,
        widget=forms.TextInput(attrs={
            'placeholder':'xxxxxx',
            'pattern': "[0-9]{6}",
            'minlength':"6",
            'title' : "Please enter a 6 digit otp.",
            'style' :"width:100%;border-radius: 5px; padding: 5px; margin: 5px 0;",
            }),
        label='Enter OTP',
        help_text='(5 minutes expiry time)'
    )

class ServicePartnerSignupForm(forms.Form):
    
    f_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'fname',
            'name' : 'fname',
            'placeholder':'',
            }),
        label='Enter First Name'
    )
    l_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'lname',
            'name' : 'lname',
            'placeholder':'',
            }),
        label='Enter Last Name'
    )    
    has_assistants = forms.BooleanField(
        label='I have Assistants',
        widget=forms.CheckboxInput(attrs={
            'class':'form-check-input',
           
        })
    )
    user_image =  forms.FileField(
        label = "Select your image",
         widget = forms.FileInput(attrs={
             'id': 'person_img',
             'name' : 'person_img',
             'accept' : "image/*",         
         }),
    )
    dob = forms.DateField(
        label = "Date of Birth",
        widget = forms.DateInput(attrs={
            'class':"form-control",
            'id' : 'dob',
            'type' : 'date',
        }),
    )
    whatsapp_no = forms.CharField(
        max_length=10,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'phone',
            'name' : 'phone',
            'pattern':'[6-9]{1}[0-9]{9}',
            'minlength' :10,

            }),
        label='Whatsapp number:'
    ) 
    address_1 = forms.CharField(
        max_length=250,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputAddress',
            'placeholder':'1234 Main St',            
            }),
        label='Address 1'
    ) 
    address_2 = forms.CharField(
        max_length=250,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputAddress2',
            'placeholder':'Apartment, studio, or floor',            
            }),
        label='Address 2'
    )
    state = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputCity',            
            }),
        label='State'
    )
    city_live = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputCity',          
            }),
        label='City'
    )
    zip = forms.CharField(
        max_length=20,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputZip',          
            }),
        label='Zip'
    )
    experience = forms.ChoiceField(choices=ServiceProvider.EXPERIENCE_CHOICES, widget=forms.Select(
        attrs={'class':'form-control',
                'id':'experience',
                'name':'experience',
                }
    ), label='Years of Experience')

    course_details = forms.CharField(widget = forms.Textarea(
        attrs={'class' : 'form-control',
                'rows' : 6,
                'id' : 'course',
                'name' :'course', 
        }),
        label='Any Course Details:',
    )
    cancelled_cheque =  forms.FileField(
        label = "Image of Cancelled Cheque",
         widget = forms.FileInput(attrs={
             'id': 'cancel_cheque',
             'name' : 'cancel_cheque',
             'accept' : "image/*",         
         }),
    )
    identity_proof =  forms.FileField(
        label = "Identity Proof(Aadhaar Card/ PAN Card/ Driving License)",
         widget = forms.FileInput(attrs={
             'id': 'identity',
             'name' : 'identity',
             'accept' : "image/*",         
         }),
    )
    bank_acno = forms.CharField(
        label = "Bank Account Number:",
        widget = forms.TextInput(attrs={
            'class':'form-control',
            'id' :'account',
            'name' : 'account',

        })
    )
    # empty_label=None
    locality = forms.ModelMultipleChoiceField(queryset=Locality.objects.all(), 
    widget = forms.SelectMultiple(
        attrs={
            'class' : 'form-control selectpicker',
            'data-live-search' : "true",
            'id' : 'service_locality',
            'name' : 'localityname',

        }
        ),
        label = 'Locality',)
    # city = forms.ChoiceField(choices=(), widget=forms.Select(
    #     attrs={'class':'form-control',
    #             'id':'service_city',
    #             }
    # ), label='City')    


class PropertyPartnerSignupForm(forms.Form):
    
    f_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'fname',
            'name' : 'fname',
            'placeholder':'',
            }),
        label='Enter First Name'
    )
    l_name = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'lname',
            'name' : 'lname',
            'placeholder':'',
            }),
        label='Enter Last Name'
    )    
       
    role = forms.ChoiceField(choices=PropertyOwner.ROLE, widget=forms.RadioSelect(
        attrs={'class':'custom-user-type-radio-list',
                'id':'role',}
    ), label='I am :')

    user_image =  forms.FileField(
        label = "Select image(Owner/Manager)",
         widget = forms.FileInput(attrs={
             'id': 'person_img',
             'name' : 'person_img',
             'accept' : "image/*",         
         }),
    )
    dob = forms.DateField(
        label = "Date of Birth",
        widget = forms.DateInput(attrs={
            'class':"form-control",
            'id' : 'dob',
            'type' : 'date',
        }),
    )
    whatsapp_no = forms.CharField(
        max_length=10,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'phone',
            'name' : 'phone',
            'pattern':'[6-9]{1}[0-9]{9}',
            'minlength' :10,

            }),
        label='Whatsapp number:'
    ) 
    address_1 = forms.CharField(
        max_length=250,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputAddress',
            'placeholder':'1234 Main St',            
            }),
        label='Address 1'
    ) 
    address_2 = forms.CharField(
        max_length=250,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputAddress2',
            'placeholder':'Apartment, studio, or floor',            
            }),
        label='Address 2'
    )
    state = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputCity',            
            }),
        label='State'
    )
    city_live = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputCity',          
            }),
        label='City'
    )
    zip = forms.CharField(
        max_length=20,
        widget=forms.TextInput(attrs={
            'class' :'form-control',
            'id' : 'inputZip',          
            }),
        label='Zip'
    )


    cancelled_cheque =  forms.FileField(
        label = "Image of Cancelled Cheque",
         widget = forms.FileInput(attrs={
             'id': 'cancel_cheque',
             'name' : 'cancel_cheque',
             'accept' : "image/*",         
         }),
    )
    identity_proof =  forms.FileField(
        label = "Identity Proof(Aadhaar Card/ PAN Card/ Driving License)",
         widget = forms.FileInput(attrs={
             'id': 'identity',
             'name' : 'identity',
             'accept' : "image/*",         
         }),
    )
    bank_acno = forms.CharField(
        label = "Bank Account Number:",
        widget = forms.TextInput(attrs={
            'class':'form-control',
            'id' :'account',
            'name' : 'account',

        })
    )


class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'username', 'id': 'exampleInputEmail1'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': '********',
            'id': 'exampleInputPassword1',
        }
))      

states = Locality.objects.values_list('state', flat=True)
states=set(states)
STATE_CHOICES = []
for state in states:
    x =(state,state)
    STATE_CHOICES.append(x)
STATE_CHOICES= tuple(STATE_CHOICES)
class PersonalBookingForm(forms.ModelForm):

    state = forms.ChoiceField(choices=STATE_CHOICES)
    # scheduled_datetime = forms.DateTimeField(input_formats=['%Y/%m/%dT%M:%H'])
    class Meta:
        model = PersonalBooking
        fields = ('first_name','last_name','email','address_1','address_2','state','zip_code','scheduled_datetime', )


class Query247Form(forms.Form):
    message = forms.CharField(
        widget=forms.Textarea(attrs={
            'class' :'form-control is-invalid',
            'id' : 'validationTextarea',
            'placeholder':'Write to us',
            'rows':2,
            }),
    ) 

class SheltownVerifyUserForm(forms.Form):
    identity_proof =  forms.FileField(
    label = "Identity Proof(Aadhaar Card/ PAN Card/ Driving License)",
        widget = forms.FileInput(attrs={
            'id': 'inputGroupFile03',
            'aria-describedby':"inputGroupFileAddon03",   
            'class' : "custom-file-input",      
        }),
    )

class SerParAvailForm(forms.Form):
    avail = forms.BooleanField(
    label='Available',
    widget=forms.CheckboxInput(attrs={
        'class':'custom-control-input',
        'id' :'customSwitch1' ,
        'toggle' : "toggle",
        'checked-data':True,
        'onclick' : "myFunction();",
        
    })
)

class PersonalServiceAddForm(forms.Form):
    persercat = forms.ModelChoiceField(queryset=PersonalServiceCategory.objects.all(),empty_label="Choose Service",to_field_name = 'name',
    widget = forms.Select(
        attrs={
            'class' : 'form-control',
            # 'data-live-search' : "true",
            'id' : 'persercat',
            'name' : 'persercat',

        }
        ),
        label = 'Category',)    

    perservice = forms.ModelChoiceField(queryset=PersonalService.objects.all(),empty_label="Choose Category",
    widget = forms.Select(
        attrs={
            'class' : 'form-control',
            # 'data-live-search' : "true",
            'id' : 'perservice',
            'name' : 'perservice',

        }
        ),
        label = 'Personal Service',)        
        #  empty_label="Choose Service",to_field_name = 'name',
        # empty_label="Choose Category", to_field_name = 'name',

class PersonalReviewForm(forms.Form):
    review = forms.CharField(widget = forms.Textarea(
        attrs={'class' : 'form-control is-invalid',
                'rows' : 6,
                'id' : 'validationTextarea',
                'placeholder':'Write Something',
                'required':True,
                'minlength' : 1,
                
        }),
        label='Write your Review',
    )    
    rating = forms.ChoiceField(choices=((1,'1 star'),(2,'2 stars'),(3,'3 stars'),(4,'4 stars'),(5,'5 stars')), widget=forms.RadioSelect(
        attrs={'class':'custom-user-type-radio-list',
                'name':'rate',
        }
    ), label='')

    perser = forms.CharField(widget = forms.TextInput(
        attrs={
            'style' : 'display:none',
            'id' : 'perserinput'
        }
    ))

class RentalReviewForm(forms.Form):
    review = forms.CharField(widget = forms.Textarea(
        attrs={'class' : 'form-control is-invalid',
                'rows' : 6,
                'id' : 'validationTextarea',
                'placeholder':'Write Something',
                'required':True,
                'minlength' : 1,
                
        }),
        label='Write your Review',
    )    
    rating = forms.ChoiceField(choices=((1,'1 star'),(2,'2 stars'),(3,'3 stars'),(4,'4 stars'),(5,'5 stars')), widget=forms.RadioSelect(
        attrs={'class':'custom-user-type-radio-list',
                'name':'rate',
        }
    ), label='')

 

class RentalBookingForm(forms.ModelForm):
    checkin_date = forms.DateInput()
    checkin_time = forms.TimeInput()
    # pg_sharing = forms.ChoiceField(choices=((1,1),(2,2),(3,3),(4,4)))
    class Meta:
        model = RentalBooking
        fields = ('first_name','last_name','email','checkin_date','checkin_time', 'pg_sharing', 'phone_number')

class JobApplyForm(forms.Form):
        resume =  forms.FileField(
        label = "Select your resume",
         widget = forms.FileInput(attrs={
             'required':True,
            #  'id': 'person_img',
            #  'name' : 'person_img',
            #  'accept' : "image/*",         
         }),
    )
class FeedbackForm(forms.Form):
    review = forms.CharField(widget = forms.Textarea(
        attrs={'class' : 'form-control is-invalid',
                'rows' : 6,
                'id' : 'validationTextarea',
                'placeholder':'Write Something',
                'required':True,
                'minlength' : 1,
                
        }),
        label='Write your Review',
    )    
