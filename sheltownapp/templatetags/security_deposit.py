from django import template
import datetime
from django.utils import timezone

register = template.Library()

@register.simple_tag
def security_deposit_tag(sec_dep_choice , rent_orig):
    '''
        sec_dep_choice is string denoting security deposit equivalent duration
        rent_orig ig the undiscounted rent 
    '''

    if sec_dep_choice=='None':
        return 0
    elif sec_dep_choice=='1 month':
        return int(rent_orig) * 1
    elif sec_dep_choice=='2 month':
        return int(rent_orig) * 2
    elif sec_dep_choice=='3 month':
        return int(rent_orig) * 3
    elif sec_dep_choice=='4 month':
        return int(rent_orig) * 4
    
    return -1 #DENOTES ERROR
          
@register.simple_tag
def reservation_amount(rent_disc):
    '''
        returns 25% of discounted rent amount as reservation amount 
    '''
    return int(int(rent_disc) * 0.25)

@register.simple_tag
def prop_type(prop_choice):
    '''
        returns property type pg/hostel | flat/appartment | house 
    '''
    return prop_choice.split(' ')[0].capitalize()

@register.simple_tag
def sharing_type(prop_choice):
    '''
        returns sharing type / bhk type after string processing
    '''
    x = prop_choice.split(' ')[1]
    y = x[0]
    z = x[1:]
    x = y + ' ' + z.upper()
    return x

@register.simple_tag
def housekeep(housekeepchoice):
    '''
        gives alias for None value of housekeeping
    '''
    if housekeepchoice=='None':
        return 'No housekeeping'
    return housekeepchoice

@register.simple_tag
def per_rating(a , b):
    '''
        cal %
    '''
    return int( (a / b) * 100)

@register.simple_tag
def five_minus_rating(a):
    '''
        returns  5 - a
    '''
    return int(5 - a)

@register.simple_tag
def removespace(string):
    '''
        removes space
    ''' 
    return string.replace(" ", "") 

@register.simple_tag
def check_past(dt):
    '''
        returns true if dt is in past 
    ''' 

    return (timezone.now() > dt)

check_past    