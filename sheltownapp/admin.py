from django.contrib import admin
from datetime import timedelta
from django.utils.translation import gettext_lazy as _
# from  .models import User,Locality,Feedback,Coupon,PropertyImages,Property,Query,PropertyReview,Amenity,AdvancedAmenity,Landmark,BlogPost,Comment,Category, How_sheltown_is_helping_us_text_in_rental_details,CommonAreaRental
# from .models import PropertyReviewLike,PropertyReviewDisLike
# from  .models import Subscriber,Landingpage_lowerimg,Landingpage_upperimg
# from  .models import PersonalServiceCategory, PersonalService, PersonalServiceSubcategory1,  PersonalServiceSubcategory2, PersonalServiceIssues
# from .models import PersonalServiceReviewLike,PersonalServiceReviewDisLike,PersonalServiceReview, PersonalCallbackRequest
# from .models import PropertyOwner, ServiceProvider,Jobsubscriber,jobapplicant,Family,IntialTeam,Aboutus,Brokage

# from .models import PersonalServiceReviewLike,PersonalServiceReviewDisLike,PersonalServiceReview, PersonalCallbackRequest,Policy,Jobs,Job_locality
from .models import *

# Register your models here.
def send_newsletter(modeladmin, request, queryset):
    for newsletter in queryset:
        newsletter.send(request)

send_newsletter.short_description = "Send selected Newsletters to all subscribers"


class BlogPostAdmin(admin.ModelAdmin):
    actions = [send_newsletter]







class BlogPostAdmin(admin.ModelAdmin):
    actions = [send_newsletter]


class InputFilter(admin.SimpleListFilter):
    template = 'sheltownapp/admin/input_filter.html'
    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return ((),)
    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice


class UIDFilter(InputFilter):
    parameter_name = 'uid'
    title = _('UID')
 
    def queryset(self, request, queryset):
        if self.value() is not None:
            uid = self.value()

            
            personal_booking = PersonalBooking.objects.get(id = uid)
            personal_service = PersonalService.objects.get(name = personal_booking.personal_service)
            dt = personal_booking.scheduled_datetime
            dt_before = dt - timedelta(hours=2)
            dt_after  = dt + timedelta(hours=2)

            a=ServiceProvider.objects.filter(services_offered_in = personal_booking.locality).filter(services_offered = personal_service).filter(is_available=True)
            b=a.exclude(bookings__scheduled_datetime__gte=dt_before , bookings__scheduled_datetime__lte=dt_after)

            return b


class ServiceProviderAdmin(admin.ModelAdmin):
    
   
    list_filter = (
        UIDFilter,
    )
def send_otp(modeladmin, request, queryset):
    for booking in queryset:
        booking.send(request)

class PersonalBookingAdmin(admin.ModelAdmin):
    list_display = ('id', 'personal_service','locality','unassigned')

    list_filter = (
        'unassigned',
    )
    actions = [send_otp]

class Query24_7_fromuserAdmin(admin.ModelAdmin):
    list_display = ('message','created_at','response')

def confirm_booking_sendotp_and_email(modeladmin, request, queryset):
    for booking in queryset:
        booking.send(request)

class RentalBookingAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'booked_property','checkin_date','checkin_time','is_confirmed')

    list_filter = (
        'is_confirmed',
    )
    actions = [confirm_booking_sendotp_and_email]


admin.site.register(User)
admin.site.register(Locality)
admin.site.register(Feedback)
admin.site.register(Coupon)
admin.site.register(Property)
admin.site.register(PropertyImages)
admin.site.register(Query)
admin.site.register(Amenity)
admin.site.register(PropertyReview)
admin.site.register(AdvancedAmenity)
admin.site.register(BlogPost,BlogPostAdmin)
admin.site.register(Comment)
admin.site.register(Category)
admin.site.register(Landmark)
admin.site.register(How_sheltown_is_helping_us_text_in_rental_details)
admin.site.register(CommonAreaRental)
admin.site.register(PropertyReviewLike)
admin.site.register(PropertyReviewDisLike)
admin.site.register(Subscriber)
admin.site.register(Landingpage_upperimg)
admin.site.register(Landingpage_lowerimg)
admin.site.register(PersonalServiceCategory)
admin.site.register(PersonalService) 
admin.site.register(PersonalServiceSubcategory1) 
admin.site.register(PersonalServiceSubcategory2)
admin.site.register(PersonalServiceIssues)
admin.site.register(PersonalServiceReviewLike)
admin.site.register(PersonalServiceReviewDisLike)
admin.site.register(PersonalServiceReview)
admin.site.register(PersonalCallbackRequest)
admin.site.register(PropertyOwner)
admin.site.register(ServiceProvider, ServiceProviderAdmin) 
admin.site.register(Policy)
admin.site.register(Jobs)
admin.site.register(Job_locality)
admin.site.register(Jobsubscriber)
admin.site.register(jobapplicant)
admin.site.register(Family)
admin.site.register(IntialTeam)
admin.site.register(Aboutus)
admin.site.register(Brokage)
admin.site.register(PersonalBooking,PersonalBookingAdmin)
admin.site.register(PersonalBookingTransaction)
admin.site.register(GSTServiceTax)
admin.site.register(Query24_7_fromuser,Query24_7_fromuserAdmin)
admin.site.register(RentalBooking ,  RentalBookingAdmin)
admin.site.register(RentalBookingTransaction)
admin.site.register(RentalRecurrentBooking)
admin.site.register(HomePageThreeImages)
admin.site.register(BecomePartner)