from django.shortcuts import render, redirect,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect
from django.views.generic import ListView
from django.core.paginator import Paginator
from .decorators import *

from .models import *
from django.forms import modelformset_factory

from django.core.mail import send_mail,BadHeaderError,EmailMessage

from django.contrib import messages
from .forms import *
import json
from django.core.serializers.json import DjangoJSONEncoder

from django.views.generic import ListView,DetailView, View

from django.template.defaultfilters import slugify
from taggit.models import Tag
from django.urls import reverse
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import random
import requests
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from .filters import PropertyFilter
import datetime
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import DateTimeField
from django.db.models.functions import Trunc
from django.db import IntegrityError
from .filters import PropertyFilter
from urllib.parse import quote_plus
from django.contrib.auth import  authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from paytmchecksum import PaytmChecksum
from decimal import *
getcontext().prec = 2
# Create your views here.
def emptyredirect(request):
    url = reverse('sheltown-index', kwargs={'modal': "landing"})
    return HttpResponseRedirect(url)
    # return render(request, 'sheltownapp/base.html', {})


# PASS modal="openmodal" to open modal automatically anyother string won't open modal
def index(request, modal="landing"):
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)

    phoneverifyform = PhoneVerificationForm()
    otpverificationform = OTPVerificationForm()
    signupform = SignUpForm()
    context={'coupons':Coupon.objects.filter(active='True'),
	'testimonials':Feedback.objects.filter(is_landingpage_verified = 'True'),
    'cities':Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'upper_images':Landingpage_upperimg.objects.all(),
    'lower_images':Landingpage_lowerimg.objects.all(),
    'phoneverifyform':phoneverifyform,
     'signupform':signupform,
    'openmodal' : False,'open1modal' : False,'open2modal' : False,'open3modal' : False,
    'otpverificationform':otpverificationform,
    'landingimage' : HomePageThreeImages.objects.first(),
    }
    if modal == "openmodal":
        context['openmodal'] = True
        render(request,'sheltownapp/index.html',context)
    else:
        context['openmodal'] = False    
    #hack is users back out to fill additional data
    if request.user.is_authenticated and request.user.custom_user.usertype == "Service Provider":
        try:
            ServiceProvider.objects.get(profile_user = request.user.custom_user)
        except ServiceProvider.DoesNotExist:
            request.user.custom_user.usertype = "User"
            request.user.custom_user.save() 
    
    if request.user.is_authenticated and request.user.custom_user.usertype == "Property Owner":
        try:
            PropertyOwner.objects.get(profile_user = request.user.custom_user)
        except PropertyOwner.DoesNotExist:
            request.user.custom_user.usertype = "User"
            request.user.custom_user.save()     


  



    if request.method == "POST":
        if 'sendotp' in request.POST:    
            form = PhoneVerificationForm(request.POST)
               
            if form.is_valid():
                    user_phone = request.POST['phone_number']
                    user_type  = request.POST['user_type']
                    url = "http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/SMS/" + user_phone + "/AUTOGEN/Welcome" #welcome is name of template
                    response = requests.request("GET", url)
                    data = response.json()
                    request.session['otp_session_data'] = data['Details']
                    request.session['user_phone'] = user_phone
                    request.session['user_type'] = user_type
                    context['open2modal'] = True
                    context['phone_no'] = user_phone
                    context['user_type'] = user_type
                    # print(user_phone + user_type)
                    # print(context['open2modal'])
                    return render(request, 'sheltownapp/index.html', context)
                    
            # except Exception as e:
            #     print(e.__cause__)
        elif 'verifyotp' in request.POST:    
            form = OTPVerificationForm(request.POST)
            context['open2modal'] = False
            if form.is_valid():
                    user_otp = request.POST['otp']
                    user_phone = request.session['user_phone']
                    user_type  = request.session['user_type']
                    context['phone_no'] = user_phone
                    context['user_type'] = user_type
                    url = "https://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/SMS/VERIFY/" + request.session['otp_session_data'] + "/" + user_otp + ""
                    response = requests.request("GET", url)
                    data = response.json()
                    if data['Status'] == 'Error' and data["Details"]=="OTP Mismatch":
                        messages.error(request, "OTP value is incorrect please try again")
                        context['open1modal'] = True
                        return render(request, 'sheltownapp/index.html', context)
                    
                    elif data["Status"] =="Success" and data["Details"]=="OTP Expired":
                        messages.error(request, "OTP expired after 5 minutes please try again")
                        context['open1modal'] = True
                        return render(request, 'sheltownapp/index.html', context)

                    elif data['Status'] == 'Error' and data["Details"]!="OTP Mismatch":
                        messages.error(request, "Some error occured on our end please try signing in after some time")
                        return render(request, 'sheltownapp/index.html', context)

                    elif data["Status"] == "Success" and data["Details"]=="OTP Matched":
                         context['open3modal'] = True
                         return render(request, 'sheltownapp/index.html', context)
                    else:
                        messages.error(request, "There was some error, please try again")
                        url = reverse('sheltown-index', kwargs={'modal': "openmodal"})
                        return HttpResponseRedirect(url)
                    context['open3modal'] = True
                    return render(request, 'sheltownapp/index.html', context)  
            else:
                context['open2modal'] = True 
                context['otpverificationform'] = form
                return render(request, 'sheltownapp/index.html', context)


                
                    # return render(request, 'sheltownapp/index.html', context)    

        elif 'resendotp' in request.POST:    
                    context['open2modal'] = False
            
                    user_phone = request.session['user_phone']
                   
                    url = "http://2factor.in/API/V1/0571a8f9-c1d3-11ea-9fa5-0200cd936042/SMS/" + user_phone + "/AUTOGEN/Welcome" #welcome is name of template
                    response = requests.request("GET", url)
                    data = response.json()
                    request.session['otp_session_data'] = data['Details']
                    request.session['user_phone'] = user_phone
                    context['open2modal'] = True
                    context['phone_no'] = user_phone
                    context['user_type'] = request.session['user_type']
                    # print(user_phone + user_type)
                    # print(context['open2modal'])
                    return render(request, 'sheltownapp/index.html', context)  

        elif 'signup' in request.POST:
                    context['open3modal'] = False
                    form = SignUpForm(request.POST)
                    if form.is_valid():
                        try:
                            defaultuser = form.save()
                        except IntegrityError:
                            messages.error(request,"An account with this username already exists")
                            context['open3modal'] = True
                            context['signupform'] = form
                            context['user_type'] = request.session['user_type']
                            context['phone_no'] = request.session['user_phone']                            
                            return render(request, 'sheltownapp/index.html', context)                            
                        defaultuser.refresh_from_db()  # load the profile instance created by the signal
                        defaultuser.email = form.cleaned_data.get('email')
                        defaultuser.custom_user.email = form.cleaned_data.get('email')
                        try:
                            defaultuser.custom_user.phone_number = request.session['user_phone']
                            defaultuser.custom_user.usertype = request.session['user_type']
                        except:
                            url = reverse('sheltown-index', kwargs={'modal': "openmodal"})
                            return HttpResponseRedirect(url)
                        
                        defaultuser.save()
                          
                        raw_password = form.cleaned_data.get('password1')
                        defaultuser = authenticate(username=defaultuser.username, password=raw_password)
                        auth_login(request, defaultuser)
                        if defaultuser.custom_user.usertype == "Property Owner":
                            return redirect('propertypartnersignup')
                        if defaultuser.custom_user.usertype == "Service Provider":
                            return redirect('servicepartnersignup')    

                    else:
                        # print(form.cleaned_data.get('email'))
                        # print(form.cleaned_data.get('username'))
                        # print(form.errors.as_data())
                        # print(form.non_field_errors)
                        context['user_type'] = request.session['user_type']
                        context['phone_no'] = request.session['user_phone']
                        context['open3modal'] = True
                        context['signupform'] = form
                        return render(request, 'sheltownapp/index.html', context)
                    return redirect('sheltown-emptyredirect')    

    else:
        pass


    return render(request,'sheltownapp/index.html',context)

@login_required
@service_partner_required
def servicepartnersignup(request):
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values('city','state','locality')
    # values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)

    context = {
    'cities':Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'form' :ServicePartnerSignupForm(),

    }
    if request.method == 'POST':
        form = ServicePartnerSignupForm(request.POST, request.FILES)

        if form.is_valid():
            print("Yes")
            request.user.custom_user.f_name = request.POST['f_name']
            request.user.custom_user.l_name = request.POST['l_name']
            request.user.custom_user.user_image = request.FILES['user_image']
            request.user.custom_user.dob = request.POST['dob']
            request.user.custom_user.address_1 = request.POST['address_1']
            request.user.custom_user.address_2 = request.POST['address_2']
            request.user.custom_user.city = request.POST['city_live']
            request.user.custom_user.state = request.POST['state']
            request.user.custom_user.zip_code = request.POST['zip']
            request.user.custom_user.identity_proof = request.FILES['identity_proof']
            request.user.custom_user.save()
            print(request.POST.getlist('locality'))
            if request.POST['has_assistants'] == 'on':
                varx = True
            else:
                varx = False
            if ServiceProvider.objects.filter(profile_user=request.user.custom_user).exists():
                partner=ServiceProvider.objects.get(profile_user=request.user.custom_user)
                partner.whatsapp_number = request.POST['whatsapp_no']
                partner.has_assistant = varx
                partner.cancelled_cheque = request.FILES['cancelled_cheque']
                partner.bank_account_number = request.POST['bank_acno']
                partner.experience_in_years = request.POST['experience']
                partner.course_details = request.POST['course_details']
                partner.services_offered_in.clear()
                partner.services_offered_in.add(request.POST['locality'])
                partner.save()

            
            else:

                partner = ServiceProvider.objects.create(profile_user = request.user.custom_user,
                                                        whatsapp_number = request.POST['whatsapp_no'],
                                                        has_assistant = varx,
                                                        cancelled_cheque = request.FILES['cancelled_cheque'],
                                                        bank_account_number = request.POST['bank_acno'],
                                                        experience_in_years = request.POST['experience'],
                                                        course_details = request.POST['course_details'])
                partner.services_offered_in.add(request.POST['locality'])
                partner.save()
            return redirect('sheltown-emptyredirect') 
        else:
            print(form.errors.as_data())
            context['form'] = form
    return render(request, 'sheltownapp/service-partner-signup.html' , context)

@login_required
@property_partner_required
def propertypartnersignup(request):
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values('city','state','locality')
    # values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)

    context = {
    'cities':Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'form' :PropertyPartnerSignupForm(),

    }
    if request.method == 'POST':
        form = PropertyPartnerSignupForm(request.POST, request.FILES)

        if form.is_valid():
            print("Yes")
            request.user.custom_user.f_name = request.POST['f_name']
            request.user.custom_user.l_name = request.POST['l_name']
            request.user.custom_user.user_image = request.FILES['user_image']
            request.user.custom_user.dob = request.POST['dob']
            request.user.custom_user.address_1 = request.POST['address_1']
            request.user.custom_user.address_2 = request.POST['address_2']
            request.user.custom_user.city = request.POST['city_live']
            request.user.custom_user.state = request.POST['state']
            request.user.custom_user.zip_code = request.POST['zip']
            request.user.custom_user.identity_proof = request.FILES['identity_proof']
            request.user.custom_user.save()

            if PropertyOwner.objects.filter(profile_user=request.user.custom_user).exists():
                partner = PropertyOwner.objects.get(profile_user=request.user.custom_user)
                partner.whatsapp_number = request.POST['whatsapp_no'],
                partner.prop_owner_designation = request.POST['role'],
                partner.cancelled_cheque = request.FILES['cancelled_cheque'],
                partner.bank_account_number = request.POST['bank_acno']
                partner.save()
            else:

                partner = PropertyOwner.objects.create(profile_user = request.user.custom_user,
                                                        whatsapp_number = request.POST['whatsapp_no'],
                                                        prop_owner_designation = request.POST['role'],
                                                        cancelled_cheque = request.FILES['cancelled_cheque'],
                                                        bank_account_number = request.POST['bank_acno'])
                partner.save()
            return redirect('sheltown-emptyredirect') 
        else:
            print(form.errors.as_data())
            context['form'] = form
    return render(request, 'sheltownapp/property-partner-signup.html' , context)

def createProperty(request):
    properties = request.user.custom_user.property_owner.properties.all
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)
    ImageFormSet = modelformset_factory(PropertyImages,
                                        form=PropertyImageForm, extra=3)
    NearbyFormSet = modelformset_factory(Landmark, form=NearbyForm, extra=4)
    if request.method == "GET" and "id" in request.GET :
        
        id = request.GET["id"]
        prop = get_object_or_404(Property, id=id)
        if "onoffswitch" in request.GET:
            prop.status = "Avaliable now"
            prop.save()
            print("Yes",prop)
        else:
            prop.status = "Not Avaliable"
            prop.save()
            print("Not",prop)
            
        properties = request.user.custom_user.property_owner.properties.all
    #'extra' means the number of photos that you can upload   ^
    if request.method == 'POST':

        postForm = PropertyForm(request.POST) 
        formset = ImageFormSet(request.POST, request.FILES,
                               queryset=PropertyImages.objects.none())
        # localityForm = LocalityForm(request.POST)
        nearbyForm = NearbyFormSet(request.POST, queryset=Landmark.objects.none())
        amenityForm = AmenityForm(request.POST)

        if postForm.is_valid() and formset.is_valid() and nearbyForm.is_valid() and amenityForm.is_valid():
            # locality_form = localityForm.save(commit=False) #saving into var instead of db
            # locality_form.save()
            locality_form =Locality.objects.filter(city=request.POST["city"],locality=request.POST["locality"])[0]
            post_form = postForm.save(commit=False)
            post_form.locality = locality_form
            amenity_form = amenityForm.save(commit=False)
            post_form.amenities = amenity_form
            amenity_form.save()
            post_form.save()



            for form in formset.cleaned_data:
                #this helps to not crash if the user   
                #do not upload all the photos
                if form:
                    image = form['image']
                    photo = PropertyImages(property=post_form, image=image)
                    photo.save()
            for form in nearbyForm.cleaned_data:
                if form:
                    name = form['name']
                    nearby = Landmark(property=post_form, name=name)
                    nearby.save()
            messages.success(request,
                             "Yeeew, check it out on the home page!")
            return HttpResponseRedirect("/")
        else:
            print(postForm.errors, formset.errors, nearbyForm.errors)
    else:
        postForm = PropertyForm()
        # localityForm = LocalityForm()s
        nearbyForm = NearbyFormSet(queryset=Landmark.objects.none())
        formset = ImageFormSet(queryset=PropertyImages.objects.none())
        amenityForm = AmenityForm()
    context ={
        'cities': Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'postForm': postForm,
    'formset': formset,
     # 'localityForm': localityForm,
      'nearbyForm': nearbyForm,
       'amenityForm' :amenityForm,
       'properties'  : properties,
       }
    return render(request, 'sheltownapp/propacctemplate/MyListings.html',context)


def about(request):
    form = MessageForm
    intial=IntialTeam.objects.all()
    fam=Family.objects.all()
    abt=Aboutus.objects.all()
    broke=Brokage.objects.all()
    if request.method == 'POST':
        postform = MessageForm(request.POST)

        if postform.is_valid():
            post_form=postform.save(commit=True)
            post_form.save()
            subject="Contact-Form Query"
            msg=postform.cleaned_data['message']
            phone_no=postform.cleaned_data['Number']
            name=postform.cleaned_data['name']
            Email=postform.cleaned_data['email']
            message = 'Name: {0}\nphone:{1}\nEmail:{2}\nMessage:{3}'.format(name, phone_no, Email,msg)
            try:
                send_mail("ContactForm-Query", message,'noreply@sheltown.com', ['info@sheltown.com'],fail_silently=False)
                messages.success(request,'YOUR MESSAGE WAS SENT SUCCESSFULLY!') 
            except Exception as e:
                print(e.message)
            return HttpResponseRedirect(request.path_info) #(request, 'sheltownapp/about-us.html')
        else:
            messages.error(request,'INVALID DETAILS!') 
    else:
        postform = MessageForm()                      

    context={
    'form':form,
    'intial':intial,
    'fam':fam,
    'abt':abt,
    'broke':broke,
    }
    return render(request,'sheltownapp/about-us.html',context)

def signup(request):
    return render(request,'sheltownapp/signup.html')

def login(request):
    return render(request,'sheltownapp/login-user.html')


class BlogPostListView(ListView):
    model = BlogPost
    template_name = 'sheltownapp/blog-details.html'

    
    def get_queryset(self):
        return BlogPost.objects.filter(published_date__lte=timezone.now().order_by('-published_date'))

class BlogPostDetailView(DetailView):
    template_name = 'sheltownapp/blog-details.html'
    model =BlogPost
    
    def get_context_data(self,*args, **kwargs):
        blogposts= BlogPost.objects.order_by('-published_date')[:4]
        context =super(BlogPostDetailView, self).get_context_data(*args, **kwargs)
        stuff=get_object_or_404(BlogPost,id=self.kwargs.get("id"))
        share_url = quote_plus(stuff.body)
        total_likes=stuff.total_likes()
        liked =False
        if stuff.likes.filter(id=self.request.user.custom_user.id).exists():
             liked =True
        disliked=False
        if stuff.dislikes.filter(id=self.request.user.custom_user.id).exists():
            disliked =True
        context["blogposts"]=blogposts
        context["total_likes"]=total_likes
        context["liked"]=liked
        context["disliked"]=disliked
        context['comments']=stuff.comments.filter(approved_comment = True)
        context["share_url"]=share_url
       # context["form"]=SubscriberForm()
        return context

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(BlogPost, id=id_)
    
def random_digits():
    return "%0.12d" % random.randint(0, 999999999999)

@csrf_exempt
def blogs(request):
    posts = BlogPost.objects.order_by('-published_date')
    common_tags=BlogPost.tags.all()
    categories = Category.objects.all()
    if request.method == 'POST':
        sub = Subscriber(email=request.POST['email'], conf_num=random_digits())
        sub.save()
        message = Mail(
            from_email='noreply@sheltown.com',
            to_emails=sub.email,
            subject='Newsletter Confirmation',
            html_content='Thank you for signing up for my email newsletter! \
                Please complete the process by \
                <a href="{}/confirm/?email={}&conf_num={}"> clicking here to \
                confirm your registration</a>.'.format(request.build_absolute_uri('/confirm/'),
                                                    sub.email,
                                                    sub.conf_num))
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        response = sg.send(message)
      
      
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'categories':categories,'email': sub.email, 'action': 'added','form': SubscriberForm(),})
    else:
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'categories':categories,'form': SubscriberForm(),})


def confirm(request):
    sub = Subscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.confirmed = True
        sub.save()
        return render(request, 'sheltownapp/blog-description-page.html', {'email': sub.email, 'action': 'confirmed'})
    else:
        return render(request, 'sheltownapp/blog-description-page.html', {'email': sub.email, 'action': 'denied'})

def delete(request):
    sub = Subscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.delete()
        return render(request, 'sheltownapp/blog-description-page.html', {'email': sub.email, 'action': 'unsubscribed',})
    else:
        return render(request, 'sheltownapp/blog-description-page.html', {'email': sub.email, 'action': 'denied',})

def blog_detail_view(request, slug):
    post = get_object_or_404(BlogPost, slug=slug)
    context = {
        'post':post,
    }
    return render(request, 'sheltownapp/blog-details.html', context)

def category_list(request):
    categories = Category.objects.all() 

    return render (request, 'sheltownapp/blog-description-page.html', {'categories': categories}) 

def category_detail(request, pk):
    print(pk)
    category = get_object_or_404(Category,title__iexact=pk)
    categories = Category.objects.all() 
    posts =BlogPost.objects.filter(category=category)
    common_tags=BlogPost.tags.all()
    if request.method == 'POST':
        sub = Subscriber(email=request.POST['email'], conf_num=random_digits())
        sub.save()
        message = Mail(
            from_email='noreply@sheltown.com',
            to_emails=sub.email,
            subject='Newsletter Confirmation',
            html_content='Thank you for signing up for my email newsletter! \
                Please complete the process by \
                <a href="{}/confirm/?email={}&conf_num={}"> clicking here to \
                confirm your registration</a>.'.format(request.build_absolute_uri('/confirm/'),
                                                    sub.email,
                                                    sub.conf_num))
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        response = sg.send(message)
      
      
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'categories':categories,'category':category,'email': sub.email, 'action': 'added','form': SubscriberForm(),})
    else:
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'categories':categories,'category':category,'form': SubscriberForm(),})

def tagged(request, slug):
    tag =get_object_or_404(Tag,slug=slug)
    posts =BlogPost.objects.filter(tags=tag)
    common_tags=BlogPost.tags.all()
    if request.method == 'POST':
        sub = Subscriber(email=request.POST['email'], conf_num=random_digits())
        sub.save()
        message = Mail(
            from_email='noreply@sheltown.com',
            to_emails=sub.email,
            subject='Newsletter Confirmation',
            html_content='Thank you for signing up for my email newsletter! \
                Please complete the process by \
                <a href="{}/confirm/?email={}&conf_num={}"> clicking here to \
                confirm your registration</a>.'.format(request.build_absolute_uri('/confirm/'),
                                                    sub.email,
                                                    sub.conf_num))
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        response = sg.send(message)
      
      
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'tag' :tag,'email': sub.email, 'action': 'added','form': SubscriberForm(),})
    else:
        return render(request,'sheltownapp/blog-description-page.html',{'common_tags':common_tags,'posts':posts,'tag' :tag,'form': SubscriberForm(),})




def LikeView(request,pk):
    post=get_object_or_404(BlogPost,id=request.POST.get("post_id"))
    liked =False
    if post.likes.filter(id=request.user.custom_user.id).exists():
       post.likes.remove(request.user.custom_user.id)
       liked=False
    else:   
       post.likes.add(request.user.custom_user.id)
       liked =True
    return HttpResponseRedirect(reverse('blog-details',args=[str(pk)]) + '#like')


def dislikeView(request,pk):
    post=get_object_or_404(BlogPost,id=request.POST.get("post_id"))
    disliked =False
    if post.dislikes.filter(id=request.user.custom_user.id).exists():
       post.dislikes.remove(request.user.custom_user.id)
       disliked=False
    else:   
       post.dislikes.add(request.user.custom_user.id)
       disliked =True
    return HttpResponseRedirect(reverse('blog-details',args=[str(pk)]))



def rental(request,city="",locality=""):
    Property.objects.set_rating(Property.objects.all())

    if city =="" and locality=="":
        q=Property.objects.all()
    else:
        q=Property.objects.filter(locality__city=city,locality__locality=locality)
    
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)

    f = PropertyFilter(request.GET, queryset=q)


    paginator = Paginator(f.qs, 25) # Show 25 per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
    'cities':Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'propimg':PropertyImages.objects.all(),
    'filter': f,
    'rentals':Property.objects.all(),
    'page_obj': page_obj
    
    }
    # return render(request, 'sheltownapp/rental-services.html', {'page_obj': page_obj})
    return render(request,'sheltownapp/rental-services.html',context)

def mobile_filter(request):
    f = PropertyFilter(request.GET, queryset=Property.objects.all())
    city_places = Locality.objects.values_list('city')
    city_json = json.dumps(list(city_places), cls=DjangoJSONEncoder)
    locality_places = Locality.objects.values_list('locality')
    locality_json = json.dumps(list(locality_places), cls=DjangoJSONEncoder)

    context = {
    'cities':Locality.objects.values('city').distinct().order_by('city'),
    'city_array':city_json,
    'locality_array':locality_json,
    'propimg':PropertyImages.objects.all(),
    'filter': f,
    'rentals':Property.objects.all(),
    # 'page_obj': page_obj
    
    }

    return render(request, 'sheltownapp/filters.html', context)

def propertydetail(request,pk):
    today = datetime.datetime.today()
    property = get_object_or_404( Property, pk = pk)
    list_helping = How_sheltown_is_helping_us_text_in_rental_details.objects.all()
    rental_coupons = Coupon.objects.filter(to_be_displayed_in_rental_details_page = True).filter(valid_from__lte = today).filter(valid_to__gte = today)

    review_1 = property.reviews.filter(rating=1).count()
    review_2 = property.reviews.filter(rating=2).count()
    review_3 = property.reviews.filter(rating=3).count()
    review_4 = property.reviews.filter(rating=4).count()
    review_5 = property.reviews.filter(rating=5).count()

    reviews = PropertyReview.objects.filter(property = property).order_by('-updated_at')[:10]

    return render(request, 'sheltownapp/rental-services-details.html', {'rental':property,
     'coupons':rental_coupons,
      'list_help':list_helping,
      'review_1':review_1,
      'review_2':review_2,
      'review_3':review_3,
      'review_4':review_4,
      'review_5':review_5,
      'reviews' : reviews, 
      'property_id' : pk, 
      })

def propertybooking(request,pk):
    bookingForm = RentalBookingForm
    prop = get_object_or_404(Property , pk = pk)
    rent = prop.rent
    locality = prop.locality.locality
    city = prop.locality.city
    security_deposit=0
    sec = prop.security_deposit
    if sec == "None":
        security_deposit=0
    elif sec == "1 month" :
        security_deposit=rent*1
    elif sec == "2 month" :
        security_deposit = rent*2
    elif sec == "3 month" :
        security_deposit = rent*3
    elif sec == "4 month" :
        security_deposit = rent*4
    total=rent+security_deposit
    promo_code=""
    discount=0
    message=""
    if request.method=='GET' and 'promocode' in request.GET:
        coupon_code = request.GET['promocode']
        if coupon_code !="":
            if Coupon.objects.filter(code=coupon_code).exists():
                promo=Coupon.objects.filter(code=coupon_code)[0]
                now = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
                if now > promo.valid_from and now < promo.valid_to:
                    if promo.active and promo.service_type == "Rental":
                        if promo.locality.locality == locality and promo.locality.city == city:
                            if total >= promo.min_amount:
                                message="Promo applied"
                                promo_code=str(promo.code)
                                promo_percent=Decimal(promo.discount)
                                discount = total*(promo_percent/100)
                                total= total - discount 
                            else:
                                message="Value of cart needs to be atleast"+str(promo.min_amount)
                        else:
                            message="Promo not available for this region"
                    else:
                        message="Promo isnt active"
                else:
                    message="Promo has Expired"
            else:
                message="Promo Doesn't Exist"
    booking_amount = total*Decimal(0.25)
    post_booking_amount = total-booking_amount
    taxes = GSTServiceTax.objects.all()[0]
    booking_with_tax_amount =booking_amount+(total*(taxes.gst_percent_rental/100))+(total*(taxes.servicetax_percent_rental/100))
    if request.method == 'POST':
        bookingForm  = RentalBookingForm(request.POST)
        if bookingForm.is_valid() :
            booking_form = bookingForm.save(commit=False)
            booking_form.booking_user=request.user.custom_user
            booking_form.promo_code = promo_code
            booking_form.booking_amount = booking_amount
            booking_form.total_amount = total
            booking_form.post_booking_amount = post_booking_amount 
            booking_form.booking_with_tax_amount = booking_with_tax_amount
            booking_form.booked_property = prop
            booking_form.property_partner = prop.property_owner
            booking_form.security_deposit = security_deposit
            booking_form.save()
            paytmParams = dict()

            paytmParams["body"] = {
                "requestType"   : "Payment",
                "mid"           : settings.MERCHANT_ID,
                "websiteName"   : "WEBSTAGING",
                "orderId"       : str(booking_form.id),
                "callbackUrl"   : "http://127.0.0.1:8000/rentalcallback/",
                "txnAmount"     : {
                    "value"     : str(float(booking_with_tax_amount)),
                    "currency"  : "INR",
                },
                "userInfo"      : {
                    "custId"    : str(request.user.custom_user.id),
                },
            }

            # Generate checksum by parameters we have in body
            # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            checksum = PaytmChecksum.generateSignature(json.dumps(paytmParams["body"]), settings.MERCHANT_KEY)

            paytmParams["head"] = {
                "signature"	: checksum
            }

            post_data = json.dumps(paytmParams)

            # for Staging
            url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+settings.MERCHANT_ID+"&orderId="+str(booking_form.id)

            # for Production
            # url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=ORDERID_98765"
            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            if response:
                txntoken = response["body"]["txnToken"]
                return render(request,"sheltownapp/paytmpayment.html",{'txnToken': txntoken,'mid': settings.MERCHANT_ID ,'orderId':str(booking_form.id)})
            else:
                return HttpResponse("No response from server")
        else:
            messages.error("Invalid Details!")
    context ={
        'total' : float(total),
        'promo_message' : message,
        'discount' : discount,
        'promo_code' : promo_code,
        'property_name' : prop.name ,
        'proptype' : prop.proptype,
        'rent' : prop.rent,
        'bookingForm' : bookingForm,
        'booking_amount' : float(booking_amount),
        'security_deposit' : float(security_deposit),
        'booking_with_tax_amount' : float(booking_with_tax_amount),
    }
    return render(request,"sheltownapp/rental-book3.html",context)

@csrf_exempt
def rentalcallback(request):
    if request.method =="POST":
        paytmParams = dict()
        paytmParams = request.POST.dict()
        paytmChecksum = paytmParams['CHECKSUMHASH']
        paytmParams.pop('CHECKSUMHASH', None)
        isVerifySignature = PaytmChecksum.verifySignature(paytmParams, settings.MERCHANT_KEY , paytmChecksum)
        
        if isVerifySignature:
            transactionParams=dict()
            transactionParams["body"] = {
            "mid" : settings.MERCHANT_ID,
            "orderId" : paytmParams["ORDERID"],
            }
            checksum = PaytmChecksum.generateSignature(json.dumps(transactionParams["body"]), settings.MERCHANT_KEY)
            transactionParams["head"] = {
                "signature"	: checksum,
            }
            post_data = json.dumps(transactionParams)
            # for Staging
            url = "https://securegw-stage.paytm.in/v3/order/status"

            # for Production
            # url = "https://securegw.paytm.in/v3/order/status"

            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            booking = RentalBooking.objects.filter(id=paytmParams["ORDERID"])[0]
            if response["body"]["resultInfo"]["resultStatus"] == "TXN_SUCCESS":
                booking.first_payment=True
                booking.save()
                transaction = RentalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                daysafter = booking.checkin_date + datetime.timedelta(days=30)
                next_payment = RentalRecurrentBooking(booking_user=booking.booking_user,second_payment=True,second_belongs_to=booking,booked_property=booking.booked_property,for_start_date=booking.checkin_date,for_end_date=daysafter,due_date=daysafter,payment_amount=booking.post_booking_amount,total_amount=booking.post_booking_amount,property_partner=booking.property_partner)
                next_payment.save()
                messages.success(request,"Payment Successful")
                return HttpResponseRedirect(reverse('bookings-user'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "PENDING":
                transaction = RentalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('bookings-user'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "TXN_FAILURE":
                transaction = RentalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('bookings-user'))

        else:
            messages.success(request,"Checsum Invalid")
            return HttpResponseRedirect(reverse('bookings-user'))

def createrecurrentbookings(request):
    bookings=RentalRecurrentBooking.objects.filter(second_payment = False, next_month_payment_generated=False, moved_out=False)
    # today = timezone.make_aware(datetime.datetime.now().date(), timezone.get_default_timezone())
    no_of_bookings=0
    for booking in bookings:
        if booking.for_end_date == datetime.datetime.now().date():
            new_start_date = booking.for_end_date + datetime.timedelta(days=1)
            new_end_date = new_start_date + datetime.timedelta(days=30)
            due_difference = booking.due_date - booking.for_start_date
            new_due_date = new_start_date + due_difference
            new_booking = RentalRecurrentBooking(booking_user=booking.booking_user,total_amount=booking.total_amount,booked_property=booking.booked_property,property_partner=booking.property_partner,for_start_date=new_start_date,for_end_date=new_end_date,payment_amount=booking.payment_amount,due_date=new_due_date)
            new_booking.save()
            booking.next_month_payment_generated = True
            booking.save()
            no_of_bookings +=1
    return HttpResponse(str(no_of_bookings)+"Number of bookings added")

@login_required(login_url='/login/')
def propertybookingrecurrent(request,pk):
    rec_booking = get_object_or_404(RentalRecurrentBooking , id = pk)
    prop = rec_booking.booked_property
    locality = prop.locality.locality
    city = prop.locality.city
    total = prop.rent
    promo_code=""
    discount=0
    message=""
    if request.method=='GET' and 'promocode' in request.GET:
        coupon_code = request.GET['promocode']
        if coupon_code !="":
            if Coupon.objects.filter(code=coupon_code).exists():
                promo=Coupon.objects.filter(code=coupon_code)[0]
                now = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
                if now > promo.valid_from and now < promo.valid_to:
                    if promo.active and promo.service_type == "Rental":
                        if promo.locality.locality == locality and promo.locality.city == city:
                            if rec_booking.second_payment == False:
                                if total >= promo.min_amount:
                                    message="Promo applied"
                                    promo_code=str(promo.code)
                                    promo_percent=Decimal(promo.discount)
                                    discount = total*(promo_percent/100)
                                    total= total - discount 
                                else:
                                    message="Value of cart needs to be atleast"+str(promo.min_amount)
                            else:
                                message="Not applicable for this payment"
                        else:
                            message="Promo not available for this region"
                    else:
                        message="Promo isnt active"
                else:
                    message="Promo has Expired"
            else:
                message="Promo Doesn't Exist"
    taxes = GSTServiceTax.objects.all()[0]
    payment_amount = total
    if rec_booking.second_payment == False:
        payment_amount =total+(total*(taxes.gst_percent_rental/100))+(total*(taxes.servicetax_percent_rental/100))
    if request.method == 'POST':
        payment_amount = float(request.POST["payment_amount"])
        total = float(request.POST["total"])
        print(total,payment_amount)
        rec_booking.payment_amount=Decimal(payment_amount)
        rec_booking.total_amount=Decimal(total)
        rec_booking.save()
        paytmParams = dict()
        
        paytmParams["body"] = {
            "requestType"   : "Payment",
            "mid"           : settings.MERCHANT_ID,
            "websiteName"   : "WEBSTAGING",
            "orderId"       : str(rec_booking.id),
            "callbackUrl"   : "http://127.0.0.1:8000/rentalrecurrentcallback/",
            "txnAmount"     : {
                "value"     : str(float(payment_amount)),
                "currency"  : "INR",
            },
            "userInfo"      : {
                "custId"    : str(request.user.custom_user.id),
            },
        }

        # Generate checksum by parameters we have in body
        # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        checksum = PaytmChecksum.generateSignature(json.dumps(paytmParams["body"]), settings.MERCHANT_KEY)

        paytmParams["head"] = {
            "signature"	: checksum
        }

        post_data = json.dumps(paytmParams)

        # for Staging
        url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+settings.MERCHANT_ID+"&orderId="+str(rec_booking.id)

        # for Production
        # url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=ORDERID_98765"
        response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
        if response:
            txntoken = response["body"]["txnToken"]
            return render(request,"sheltownapp/paytmpayment.html",{'txnToken': txntoken,'mid': settings.MERCHANT_ID ,'orderId':str(rec_booking.id)})
        else:
            return HttpResponse("No response from server")
    context ={
        'total' : float(total),
        'promo_message' : message,
        'discount' : discount,
        'promo_code' : promo_code,
        'property_name' : prop.name ,
        'proptype' : prop.proptype,
        'rent' : prop.rent,
        'payment_amount' : float(payment_amount),}
    return render(request, "sheltownapp/rental-book-recurrent.html",context)

@csrf_exempt
def rentalrecurrentcallback(request):
    if request.method =="POST":
        paytmParams = dict()
        paytmParams = request.POST.dict()
        paytmChecksum = paytmParams['CHECKSUMHASH']
        paytmParams.pop('CHECKSUMHASH', None)
        isVerifySignature = PaytmChecksum.verifySignature(paytmParams, settings.MERCHANT_KEY , paytmChecksum)
        
        if isVerifySignature:
            transactionParams=dict()
            transactionParams["body"] = {
            "mid" : settings.MERCHANT_ID,
            "orderId" : paytmParams["ORDERID"],
            }
            checksum = PaytmChecksum.generateSignature(json.dumps(transactionParams["body"]), settings.MERCHANT_KEY)
            transactionParams["head"] = {
                "signature"	: checksum,
            }
            post_data = json.dumps(transactionParams)
            # for Staging
            url = "https://securegw-stage.paytm.in/v3/order/status"

            # for Production
            # url = "https://securegw.paytm.in/v3/order/status"

            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            booking = RentalRecurrentBooking.objects.filter(id=paytmParams["ORDERID"])[0]
            if response["body"]["resultInfo"]["resultStatus"] == "TXN_SUCCESS":
                booking.payment_complete=True
                booking.save()
                transaction = RentalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking_recurrent=booking)
                transaction.save()
                messages.success(request,"Payment Successful")
                return HttpResponseRedirect(reverse('bookings-user'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "PENDING":
                transaction = RentalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking_recurrent=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('bookings-user'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "TXN_FAILURE":
                transaction = RentalBookingTransaction(ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],
                transaction_json=str(response), booking_recurrent=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('bookings-user'))
               

        else:
            messages.success(request,"Checksum Invalid")
            return HttpResponseRedirect(reverse('bookings-user'))


def propertydetailreview(request, pk):
    today = datetime.datetime.today()
    property = get_object_or_404( Property, pk = pk)
    
    reviews = PropertyReview.objects.filter(property = property).order_by('-updated_at')

    return render(request, 'sheltownapp/rental-reviews-page.html', {'rental':property,
      'reviews' : reviews
      })    

class UpdatePropertyReviewVote(LoginRequiredMixin, View):
    login_url = ''
    redirect_field_name = ''

    def get(self, request, *args, **kwargs):

        review_id = self.kwargs.get('review_id', None)
        opinion = self.kwargs.get('opinion', None) # like or dislike button clicked

        review = get_object_or_404(PropertyReview, id=review_id)

        try:
            # If child DisLike model doesnot exit then create
            review.dis_likes
        except PropertyReview.dis_likes.RelatedObjectDoesNotExist as identifier:
            PropertyReviewDisLike.objects.create(review = review)

        try:
            # If child Like model doesnot exit then create
            review.likes
        except PropertyReview.likes.RelatedObjectDoesNotExist as identifier:
            PropertyReviewLike.objects.create(review = review)

        if opinion.lower() == 'like':

            if request.user.custom_user in review.likes.users.all():
                review.likes.users.remove(request.user.id)
            else:    
                review.likes.users.add(request.user.id)
                review.dis_likes.users.remove(request.user.id)

        elif opinion.lower() == 'dis_like':

            if request.user.custom_user in review.dis_likes.users.all():
                review.dis_likes.users.remove(request.user.id)
            else:    
                review.dis_likes.users.add(request.user.id)
                review.likes.users.remove(request.user.id)
        else:
            url = reverse('property-detail', kwargs={'pk': review.property.pk})
            return HttpResponseRedirect(url + '#review' + str(review.pk))
            
        url = reverse('property-detail', kwargs={'pk': review.property.pk})
        return HttpResponseRedirect(url + '#review' + str(review.pk))

class UpdatePropertyReviewAllVote(LoginRequiredMixin, View):
    login_url = ''
    redirect_field_name = ''

    def get(self, request, *args, **kwargs):

        review_id = self.kwargs.get('review_id', None)
        opinion = self.kwargs.get('opinion', None) # like or dislike button clicked

        review = get_object_or_404(PropertyReview, id=review_id)

        try:
            # If child DisLike model doesnot exit then create
            review.dis_likes
        except PropertyReview.dis_likes.RelatedObjectDoesNotExist as identifier:
            PropertyReviewDisLike.objects.create(review = review)

        try:
            # If child Like model doesnot exit then create
            review.likes
        except PropertyReview.likes.RelatedObjectDoesNotExist as identifier:
            PropertyReviewLike.objects.create(review = review)

        if opinion.lower() == 'like':

            if request.user.custom_user in review.likes.users.all():
                review.likes.users.remove(request.user.id)
            else:    
                review.likes.users.add(request.user.id)
                review.dis_likes.users.remove(request.user.id)

        elif opinion.lower() == 'dis_like':

            if request.user.custom_user in review.dis_likes.users.all():
                review.dis_likes.users.remove(request.user.id)
            else:    
                review.dis_likes.users.add(request.user.id)
                review.likes.users.remove(request.user.id)
        else:
            url = reverse('property-detail-review', kwargs={'pk': review.property.pk})
            return HttpResponseRedirect(url + '#review' + str(review.pk))
            
        url = reverse('property-detail-review', kwargs={'pk': review.property.pk})
        return HttpResponseRedirect(url + '#review' + str(review.pk))

def add_comment(request,pk):
    post = get_object_or_404(BlogPost,pk=pk)
    new_comment = None
    comment_form = CommentForm

    if request.method == 'POST':
        comment_form =CommentForm(request.POST)

        if comment_form.is_valid():
            new_comment=comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
            messages.success(request,"YOUR COMMENT WAS ADDED")
            return redirect('blog-details',id=post.id)
        else:
            messages.error(request,"INVALID DETAILS")
    else:
        comment_form = CommentForm()

    return render(request,'sheltownapp/commentform.html',{'comment_form': comment_form})


def personalservice(request,city="",locality=""):
    persercat = PersonalServiceCategory.objects.all()
    if PersonalServiceIssues.objects.filter(locality__city=city,locality__locality=locality).exists():

        return render(request,'sheltownapp/personal-services-1.html', {
            'personalcategory' : persercat
        })
    else:
        return HttpResponse("<h1> No Personal Services Page in this Locality  </h1>")   

def personalservicecategory(request,city="",locality="",service="Personal Care"):
    today = datetime.datetime.today()
    PerSer = PersonalService.objects.filter(category__name=service)
    per_issue = PersonalServiceIssues.objects.filter(locality__city=city,locality__locality=locality)
    PersonalCategory = get_object_or_404(PersonalServiceCategory, name=service)
    callbackform = PersonalCallbackRequestForm()
    context={
        'personalcategory' : service,
        'personalservice'  : PerSer,
        'offers'           : Coupon.objects.filter(to_be_displayed_in_personalised_page = True).filter(valid_from__lte = today).filter(valid_to__gte = today) ,
        'reviews'          : PersonalServiceReview.objects.filter(personalservice__category__name = service).order_by('-updated_at')[:10],
        'callbackform'     : callbackform,
        'city'             : city,
        'locality'         : locality,
        'per_issue'        : per_issue,
    }
    
    
    if request.method == "POST":
        if 'callbackrequest' in request.POST:    
            form = PersonalCallbackRequestForm(request.POST)
            try:    
                if form.is_valid():
                    callbackrequest = form.save(commit=False)
                    callbackrequest.category = PersonalCategory
                    callbackrequest.save()
                    messages.success(request,"WE WILL GET BACK TO YOU SOON!")
                    return redirect('sheltown-personalservicecategory', service=service)
            except IntegrityError:
                messages.error(request,"Your request has already being registered")
        else:
                messages.error(request,"INVALID PHONE NUMBER")                
    else:
        pass
     
    return render(request,'sheltownapp/personal_services_personal.html',context)


def personalservicecategoryreview(request,city="",locality="", service="Personal Care"):
   
    PerSer = PersonalService.objects.filter(category__name=service)
    reviews = PersonalServiceReview.objects.filter(personalservice__category__name = service).order_by('-updated_at')

    return render(request, 'sheltownapp/personal-reviews-page.html', 
    {
        'personalcategory' : service,
        'personalservice'  : PerSer,
        'reviews' : reviews,
        'city'   : city,
        'locality':locality,
      })    

class UpdatePersonalReviewVote(LoginRequiredMixin, View):
    login_url = ''
    redirect_field_name = ''

    def get(self, request, *args, **kwargs):

        review_id = self.kwargs.get('review_id', None)
        opinion = self.kwargs.get('opinion', None) # like or dislike button clicked
        city = self.kwargs.get('city', None)
        locality = self.kwargs.get('locality', None)
        review = get_object_or_404(PersonalServiceReview, id=review_id)

        try:
            # If child DisLike model doesnot exit then create
            review.dis_likes
        except PersonalServiceReview.dis_likes.RelatedObjectDoesNotExist as identifier:
            PersonalServiceReviewDisLike.objects.create(review = review)

        try:
            # If child Like model doesnot exit then create
            review.likes
        except PersonalServiceReview.likes.RelatedObjectDoesNotExist as identifier:
            PersonalServiceReviewLike.objects.create(review = review)

        if opinion.lower() == 'like':

            if request.user.custom_user in review.likes.users.all():
                review.likes.users.remove(request.user.id)
            else:    
                review.likes.users.add(request.user.id)
                review.dis_likes.users.remove(request.user.id)

        elif opinion.lower() == 'dis_like':

            if request.user.custom_user in review.dis_likes.users.all():
                review.dis_likes.users.remove(request.user.id)
            else:    
                review.dis_likes.users.add(request.user.id)
                review.likes.users.remove(request.user.id)
        else:
            url = reverse('sheltown-personalservicecategory', kwargs={'service': review.personalservice.category.name,'city':city,'locality':locality})
            return HttpResponseRedirect(url + '#review' + str(review.pk))
            
        url = reverse('sheltown-personalservicecategory', kwargs={'service': review.personalservice.category.name,'city':city,'locality':locality})
        return HttpResponseRedirect(url + '#review' + str(review.pk))

class UpdatePersonalReviewAllVote(LoginRequiredMixin, View):
    login_url = ''
    redirect_field_name = ''

    def get(self, request, *args, **kwargs):

        review_id = self.kwargs.get('review_id', None)
        opinion = self.kwargs.get('opinion', None) # like or dislike button clicked
        city = self.kwargs.get('city', None)
        locality = self.kwargs.get('locality', None)
        review = get_object_or_404(PersonalServiceReview, id=review_id)

        try:
            # If child DisLike model doesnot exit then create
            review.dis_likes
        except PersonalServiceReview.dis_likes.RelatedObjectDoesNotExist as identifier:
            PersonalServiceReviewDisLike.objects.create(review = review)

        try:
            # If child Like model doesnot exit then create
            review.likes
        except PersonalServiceReview.likes.RelatedObjectDoesNotExist as identifier:
            PersonalServiceReviewLike.objects.create(review = review)

        if opinion.lower() == 'like':

            if request.user.custom_user in review.likes.users.all():
                review.likes.users.remove(request.user.id)
            else:    
                review.likes.users.add(request.user.id)
                review.dis_likes.users.remove(request.user.id)

        elif opinion.lower() == 'dis_like':

            if request.user.custom_user in review.dis_likes.users.all():
                review.dis_likes.users.remove(request.user.id)
            else:    
                review.dis_likes.users.add(request.user.id)
                review.likes.users.remove(request.user.id)
        else:
            url = reverse('sheltown-personalservicecategory-review', kwargs={'service': review.personalservice.category.name,'city':city,'locality':locality})
            return HttpResponseRedirect(url + '#review' + str(review.pk))
            
        url = reverse('sheltown-personalservicecategory-review', kwargs={'service': review.personalservice.category.name,'city':city,'locality':locality})
        return HttpResponseRedirect(url + '#review' + str(review.pk))  

@login_required
def personalbooking(request,city="",locality=""):
    bookingForm = PersonalBookingForm
    name_price = request.GET.getlist('name')
    name=[]
    price=[]
    for n in name_price:
        na,pr = n.split(',')
        name.append(na)
        price.append(pr)
    total = Decimal(request.GET['total'])
    service = request.GET['perser']
    subcat = request.GET['subcat']
    subcat2 =request.GET['subcat2']
    products = []
    issues={}
    for i in range(len(name)):
        products.append({
                    'name' : name[i],
                    'price' : price[i]
                    })
        issues[name[i]]=price[i]
        
    promo_code=""
    discount=0
    message=""
    if request.method=='GET' and 'promocode' in request.GET:
        coupon_code = request.GET['promocode']
        
        if coupon_code !="":
            if Coupon.objects.filter(code=coupon_code).exists():
                promo=Coupon.objects.filter(code=coupon_code)[0]
                now = timezone.make_aware(datetime.datetime.now(), timezone.get_default_timezone())
                if now > promo.valid_from and now < promo.valid_to:
                    if promo.active and promo.service_type == "Personalized":
                        if promo.locality.locality == locality and promo.locality.city == city:
                            if total >= promo.min_amount:
                                message="Promo applied"
                                promo_code=str(promo.code)
                                promo_percent=Decimal(promo.discount)
                                discount = total*(promo_percent/100)
                                total= total - discount 
                            else:
                                message="Value of cart needs to be atleast"+str(promo.min_amount)
                        else:
                            message="Promo not available for this region"
                    else:
                        message="promo isnt active"
                else:
                    message="Promo has Expired"
            else:
                message="Promo Doesn't Exist"
    booking_amount = total*Decimal(0.25)
    post_service_amount = total-booking_amount
    taxes = GSTServiceTax.objects.all()[0]
    payment_amount =booking_amount+(total*(taxes.gst_percent_personal/100))+(total*(taxes.servicetax_percent_personal/100))
    if request.method == 'POST':
        bookingForm  = PersonalBookingForm(request.POST)
        if bookingForm.is_valid() :
            booking_form = bookingForm.save(commit=False)
            booking_form.booking_user=request.user.custom_user
            booking_form.promo_code = promo_code
            booking_form.booking_amount = booking_amount
            booking_form.total_amount = total
            booking_form.post_service_amount = post_service_amount
            booking_form.first_payment_amount = payment_amount
            booking_form.personal_service = service
            booking_form.personal_subcat1 = subcat
            booking_form.personal_subcat2 = subcat2
            booking_form.personal_service_issue = json.dumps(issues)
            booking_form.locality = Locality.objects.filter(locality=locality,city=city)[0]
            booking_form.save()
            paytmParams = dict()

            paytmParams["body"] = {
                "requestType"   : "Payment",
                "mid"           : settings.MERCHANT_ID,
                "websiteName"   : "WEBSTAGING",
                "orderId"       : str(booking_form.id),
                "callbackUrl"   : "http://127.0.0.1:8000/personalcallback/",
                "txnAmount"     : {
                    "value"     : str(float(payment_amount)),
                    "currency"  : "INR",
                },
                "userInfo"      : {
                    "custId"    : str(request.user.custom_user.id),
                },
            }

            # Generate checksum by parameters we have in body
            # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            checksum = PaytmChecksum.generateSignature(json.dumps(paytmParams["body"]), settings.MERCHANT_KEY)

            paytmParams["head"] = {
                "signature"	: checksum
            }

            post_data = json.dumps(paytmParams)

            # for Staging
            url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+settings.MERCHANT_ID+"&orderId="+str(booking_form.id)

            # for Production
            # url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=ORDERID_98765"
            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            
            txntoken = response["body"]["txnToken"]
            return render(request,"sheltownapp/paytmpayment.html",{'txnToken': txntoken,'mid': settings.MERCHANT_ID ,'orderId':str(booking_form.id)})

        else:
            messages.error(request,'INVALID DETAILS!') 

    context = {
        'products' : products,
        'total' : total,
        'service' : service,
        'subcat'  : subcat,
        'subcat2' : subcat2,
        'promo_message' : message,
        'discount' : discount,
        'promo_code' : promo_code,
        'bookingForm' : bookingForm,
        'booking_amount' : float(booking_amount),
        'payment_amount' : float(payment_amount),
    }
    return render(request,'sheltownapp/3.html.html', context)

@csrf_exempt
def personalcallback(request):
    if request.method =="POST":
        paytmParams = dict()
        paytmParams = request.POST.dict()
        paytmChecksum = paytmParams['CHECKSUMHASH']
        paytmParams.pop('CHECKSUMHASH', None)
        isVerifySignature = PaytmChecksum.verifySignature(paytmParams, settings.MERCHANT_KEY , paytmChecksum)
        
        if isVerifySignature:
            transactionParams=dict()
            transactionParams["body"] = {
            "mid" : settings.MERCHANT_ID,
            "orderId" : paytmParams["ORDERID"],
            }
            checksum = PaytmChecksum.generateSignature(json.dumps(transactionParams["body"]), settings.MERCHANT_KEY)
            transactionParams["head"] = {
                "signature"	: checksum,
            }
            post_data = json.dumps(transactionParams)
            # for Staging
            url = "https://securegw-stage.paytm.in/v3/order/status"

            # for Production
            # url = "https://securegw.paytm.in/v3/order/status"

            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            booking = PersonalBooking.objects.filter(id=paytmParams["ORDERID"])[0]
            if response["body"]["resultInfo"]["resultStatus"] == "TXN_SUCCESS":
                booking.first_payment=True
                booking.save()
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"Payment Sucessful")
                return HttpResponseRedirect(reverse('userappointment'))

            elif response["body"]["resultInfo"]["resultStatus"]  == "PENDING":
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('userappointment'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "TXN_FAILURE":
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=paytmParams["ORDERID"],
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('userappointment'))

        else:
            messages.success(request,"Checksum Invalid")
            return HttpResponseRedirect(reverse('userappointment'))

def testimonial(request):
    feedbackForm = FeedbackForm()
    customerreview=Feedback.objects.filter(is_testimonial_verified='True')
    if request.method == "GET" and "rate" in request.GET:

        review = request.GET['review']
        rating = request.GET['rate']
        feed_back = Feedback(content=review, rating=rating,user=request.user.custom_user)
        feed_back.save()
        messages.success(request, "Successfully submitted") 

    return render(request,'sheltownapp/testimonials.html',{'customerreview':customerreview,'feedbackForm' : feedbackForm})

def policy_list(request):
    policy=Policy.objects.all()
    return render(request,'sheltownapp/policy_page.html',{'policy':policy,})

def policy_detail(request,pk):
    policies = get_object_or_404(Policy,policy_name=pk)
    pol=Policy.objects.filter(policy_name=policies)
    policy=Policy.objects.all()
    return render(request,'sheltownapp/policy_page.html',{'policies':policies,'pol':pol,'policy':policy,})

def jobs(request):
    jobs            = Jobs.objects.filter(lastdate__gte = datetime.datetime.now())
    job_types      = Job_locality.objects.values('job_type').distinct()
    job_localities = Job_locality.objects.values('job_locality').distinct()
    # if request.method =='POST':
    #     if 'applybtn' in request.POST: 
    #         resume=ResumeForm()   
    #         if resume.is_valid():
    #             p = resume.cleaned_data['Resume']    
    #             cv=jobapplicant(Resume=p,applied=True,user=request.user)
    #             cv.save()
    #         messages.success(request, "Successfully applied") 

    if request.method == 'POST' and 'signup' in request.POST :
        sub = Jobsubscriber(email=request.POST['email'], conf_num=random_digits())
        sub.save()
        message = Mail(
            from_email='noreply@sheltown.com',
            to_emails=sub.email,
            subject='Job alerts Confirmation',
            html_content='Thank you for signing up for email Jobalerts! \
                Please complete the process by \
                <a href="{}/confirm/?email={}&conf_num={}"> clicking here to \
                confirm your registration</a>.'.format(request.build_absolute_uri('/alertconfirm/'),
                                                    sub.email,
                                                    sub.conf_num))
        sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
        response = sg.send(message)
      
      
        return render(request,'sheltownapp/work-with-us.html',{'jobs':jobs,'job_types':job_types,'job_localities':job_localities,'email': sub.email, 'action': 'added','form': AlertForm()})
    else:
        return render(request,'sheltownapp/work-with-us.html',{'jobs':jobs,'job_types':job_types,'job_localities':job_localities,'form': AlertForm()})


def jobalertconfirm(request):
    sub = Jobsubscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.confirmed = True
        sub.save()
        return render(request, 'sheltownapp/work-with-us.html', {'email': sub.email, 'form': AlertForm(),'action': 'confirmed'})
    else:
        return render(request, 'sheltownapp/work-with-us.html', {'email': sub.email, 'form': AlertForm(),'action': 'denied'})

def jobalertdelete(request):
    sub = Jobsubscriber.objects.get(email=request.GET['email'])
    if sub.conf_num == request.GET['conf_num']:
        sub.delete()
        return render(request, 'sheltownapp/work-with-us.html', {'email': sub.email,'form': AlertForm(), 'action': 'unsubscribed',})
    else:
        return render(request, 'sheltownapp/work-with-us.html', {'email': sub.email,'form': AlertForm(), 'action': 'denied',})

@login_required
def job_apply(request,pk):
    job = get_object_or_404(Jobs, pk=pk)
    context={
        'job' : job,
        'form' : JobApplyForm()
    }

    
    if request.method=="POST":
        form = JobApplyForm(request.POST,request.FILES)
        if form.is_valid():
            resume = request.FILES['resume']
            app = jobapplicant.objects.create(resume=resume, user=request.user.custom_user, job=job)    
            app.save()
            messages.success(request, "Your application is successfully registered")
            return HttpResponseRedirect(reverse('join'))
        else:
            context['form'] = form    
    
    return render(request, 'sheltownapp/work-with-us-apply.html', context)
    
    
    # if request.method == "POST":
    #     job_type = request.POST['job_type']
    #     job_locality = request.POST['job_locality']
    #     job = Jobs.objects.filter(job_type=job_type,job_locality=job_locality)
    #     context = {
    #      'job':job,
    #     }
    # else:
    #     messages.error(request,"INVALID PHONE NUMBER")  
    #     context = {}
    

@login_required
@service_partner_required
def servicepartnermyacc(request):
    return render(request, 'sheltownapp/seracctemp/service_partner.html', {})

@login_required
@service_partner_required
def servicepartnermytransactions(request):
    bookings = request.user.custom_user.service_provider.bookings.all
    return render(request, 'sheltownapp/seracctemp/transaction.html', { 'bookings' : bookings,})

@login_required
@property_partner_required
def propertypartnermyacc(request):
    return render(request, 'sheltownapp/propacctemplate/property_partner.html', {})

@login_required
@property_partner_required
def propertypartnermytransactions(request):
    bookings = request.user.custom_user.property_owner.bookings.all
    rec_bookings = request.user.custom_user.property_owner.rec_bookings.all
    context ={
        'bookings' : bookings,
        'rec_bookings' : rec_bookings,
    }
    return render(request, 'sheltownapp/propacctemplate/transaction.html',context)


@login_required
@normal_user_required
def usermyacc(request):
    return render(request, 'sheltownapp/useracctemp/user.html', {})

@login_required
@normal_user_required
def usermytransactions(request):
    rental_bookings = request.user.custom_user.rentalbookings.all
    rec_bookings = request.user.custom_user.rentalrecbookings.all
    appointments = request.user.custom_user.appointments.all

    context ={
        'rental_bookings' : rental_bookings,
        'rec_bookings' : rec_bookings,
        'appointments' : appointments,
    }
    return render(request, 'sheltownapp/useracctemp/transaction.html', context)

@login_required
def queryuseraccount(request):
    queries = Query24_7_fromuser.objects.filter(user=request.user.custom_user).order_by('-created_at')
    form = Query247Form()
    context = {
        'queries': queries,
        'form' : form,
    }
    if request.method == "POST":
            
            form = Query247Form(request.POST)
               
            if form.is_valid():
                message = request.POST["message"]
                query = Query24_7_fromuser.objects.create(message=message,user=request.user.custom_user,response="")
                query.save()
                return HttpResponseRedirect(request.path_info)
            else:
                context['form'] = form
    return render(request,'sheltownapp/seracctemp/help_service.html',context)

@login_required
def verifyuseraccount(request):
    form = SheltownVerifyUserForm()
    context = {
        'form' : form,
    }
    if request.method == "POST":
            
            form = SheltownVerifyUserForm(request.POST,request.FILES)
               
            if form.is_valid():
                fl = request.FILES["identity_proof"]
                request.user.custom_user.identity_proof = fl
                request.user.custom_user.save() 
                messages.success(request,
                             "Identity Proof Has been uploaded,please wait for sheltown verification")
                return HttpResponseRedirect(request.path_info)
            else:
                context['form'] = form
    return render(request,'sheltownapp/seracctemp/verification.html',context)

@login_required
@service_partner_required
def servicepartnerlisting(request):
    ser_cat = PersonalService.objects.values_list('category__name')
    cat_json = json.dumps(list(ser_cat), cls=DjangoJSONEncoder)
    ser = PersonalService.objects.values('name','category__name','id')
    ser_json = json.dumps(list(ser), cls=DjangoJSONEncoder)

    form = PersonalServiceAddForm()
    context = {
        'cat' : PersonalService.objects.values('category__name').distinct().order_by('category__name'),
        'cat_array' : cat_json,
        'ser_array' : ser_json,
        'form' : form,
    }
    if request.method == "POST":
        if 'availtoggle' in request.POST: 

                if request.user.custom_user.service_provider.is_available:
                    request.user.custom_user.service_provider.is_available = False
                else:
                    request.user.custom_user.service_provider.is_available = True 
                request.user.custom_user.service_provider.save()                   
                return HttpResponseRedirect(request.path_info)
                
                # else:
                    # context['form'] = form

        elif 'perser' in request.POST: 
                form =  PersonalServiceAddForm(request.POST)
                

                if form.is_valid():
                    perser = request.POST["perservice"]
                  
                    request.user.custom_user.service_provider.services_offered.add(perser)
                    request.user.custom_user.service_provider.save()

                                  
                    return HttpResponseRedirect(request.path_info)
                
                else:
                 
                    context['form'] = form
    return render(request,'sheltownapp/seracctemp/service_listing.html',context)

@login_required
@service_partner_required
def servicepartnerbooking(request):

    comp_booking = request.user.custom_user.service_provider.bookings.filter(is_completed=True).order_by('-scheduled_datetime')
    upcoming_booking = request.user.custom_user.service_provider.bookings.filter(is_completed=False).order_by('-scheduled_datetime')
    context={
        'comp_booking' : comp_booking,
        'upcoming_booking' : upcoming_booking,
    }
   
    return render(request,'sheltownapp/seracctemp/service_user.html',context)

@login_required
@service_partner_required
def servicepartnerbookingpaid(request,uuid="",method=""):

    if uuid=="" or method=="":
        return HttpResponseRedirect(reverse('servicepartnerbooking'))
    else:
        booking=get_object_or_404(PersonalBooking,id=uuid)
        booking.is_completed = True
        booking.save()
        if method=='cash':
            booking.second_payment = True
            booking.second_payment_mode = 'cash'
            booking.save()
            PersonalBookingTransaction.objects.create(booking=booking, PAYMENTMODE="cash",TXNAMOUNT=booking.post_service_amount)
            messages.success(request,"cash transaction is successfully recorded with us")
        elif method == 'paytm':
            # paytmParams = dict()
            # paytmParams["body"] = {
            #     "merchantRequestId" : str(booking.id),
            #     "mid"             : settings.MERCHANT_ID,
            #     "linkType"        : "GENERIC",
            #     "linkDescription" : "Test Payment",
            #     "linkName"        : "Test",
            #     "amount"          : str(float(booking.post_service_amount)),
            #     "sendSms"         : "True",
            #     "customerContact" : {
            #         "customerName"   : str(booking.booking_user.f_name),
            #         "customerMobile" : str(booking.booking_user.phone_number),    
            #     },
            #     "statusCallbackUrl" : "http://127.0.0.1:8000/personallinkcallback/",
            # }
            # paytmParams["body"] = {
            #     "mid"             : settings.MERCHANT_ID,
            #     "linkType"        : "GENERIC",
            #     "linkDescription" : "Test Payment",
            #     "linkName"        : "Test",
            # }
            # Generate checksum by parameters we have in body
            # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
            # checksum = PaytmChecksum.generateSignature(json.dumps(paytmParams["body"]), settings.MERCHANT_KEY)

            # paytmParams["head"] = {
            #     "tokenType"       : "AES",
            #     "signature"       : checksum,
            # }

            # post_data = json.dumps(paytmParams)

            # # for Staging
            # url = "https://securegw-stage.paytm.in/link/create"

            # # for Production
            # # url = "https://securegw.paytm.in/link/create"

            # response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            # print(response)
            booking.second_payment_mode = 'paytm'
            booking.save()
            messages.success(request,"please wait for customer to complete transaction successfully")
        return HttpResponseRedirect(reverse('servicepartnerbooking'))
   
    return render(request,'sheltownapp/seracctemp/service_user.html',context)

@csrf_exempt
def personallinkcallback(request):
    if request.method =="POST":
        paytmParams = dict()
        paytmParams = request.POST.dict()
        paytmChecksum = paytmParams['CHECKSUMHASH']
        paytmParams.pop('CHECKSUMHASH', None)
        isVerifySignature = PaytmChecksum.verifySignature(paytmParams, settings.MERCHANT_KEY , paytmChecksum)
        
        if isVerifySignature:
            order_id = paytmParams["ORDERID"]
            order_id = order_id[:-2]
            transactionParams=dict()
            transactionParams["body"] = {
            "mid" : settings.MERCHANT_ID,
            "orderId" : paytmParams["ORDERID"],
            }
            checksum = PaytmChecksum.generateSignature(json.dumps(transactionParams["body"]), settings.MERCHANT_KEY)
            transactionParams["head"] = {
                "signature"	: checksum,
            }
            post_data = json.dumps(transactionParams)
            # for Staging
            url = "https://securegw-stage.paytm.in/v3/order/status"

            # for Production
            # url = "https://securegw.paytm.in/v3/order/status"

            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            booking = PersonalBooking.objects.filter(id=order_id)[0]
            if response["body"]["resultInfo"]["resultStatus"] == "TXN_SUCCESS":
                booking.second_payment=True
                booking.save()
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=order_id,
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"Payment Successful")
                return HttpResponseRedirect(reverse('userappointment'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "PENDING":
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=order_id,
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('userappointment'))
            elif response["body"]["resultInfo"]["resultStatus"]  == "TXN_FAILURE":
                transaction = PersonalBookingTransaction(TXNID=paytmParams["TXNID"],ORDERID=order_id,
                BANKTXNID=paytmParams["BANKTXNID"],TXNAMOUNT=paytmParams["TXNAMOUNT"],STATUS=paytmParams["STATUS"],
                RESPCODE=paytmParams["RESPCODE"],TXNDATE=str(paytmParams["TXNDATE"]),PAYMENTMODE=paytmParams["PAYMENTMODE"],
                transaction_json=str(response), booking=booking)
                transaction.save()
                messages.success(request,"CODE"+response["body"]["resultInfo"]["resultCode"]+"\n"+response["body"]["resultInfo"]["resultMsg"])
                return HttpResponseRedirect(reverse('userappointment'))

        else:
	        return HttpResponse("Checksum Invalid")

@login_required
@normal_user_required
def userappointment(request):
    comp_booking = request.user.custom_user.appointments.filter(is_completed=True).order_by('-scheduled_datetime')
    upcoming_booking = request.user.custom_user.appointments.filter(is_completed=False).order_by('-scheduled_datetime')
    context={
        'comp_booking' : comp_booking,
        'upcoming_booking' : upcoming_booking,
        'form' : PersonalReviewForm(),
    }
    if request.method == "POST" and "payid" in request.POST:
        payid = request.POST["payid"]
        booking = get_object_or_404(PersonalBooking, id=payid)
        paytmParams = dict()

        paytmParams["body"] = {
                "requestType"   : "Payment",
                "mid"           : settings.MERCHANT_ID,
                "websiteName"   : "WEBSTAGING",
                "orderId"       : str(booking.id)+"_2",
                "callbackUrl"   : "http://127.0.0.1:8000/personallinkcallback/",
                "txnAmount"     : {
                    "value"     : str(float(booking.post_service_amount)),
                    "currency"  : "INR",
                },
                "userInfo"      : {
                    "custId"    : str(request.user.custom_user.id),
                },
            }

        # Generate checksum by parameters we have in body
        # Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
        checksum = PaytmChecksum.generateSignature(json.dumps(paytmParams["body"]), settings.MERCHANT_KEY)

        paytmParams["head"] = {
                "signature"	: checksum
            }

        post_data = json.dumps(paytmParams)

        # for Staging
        url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid="+settings.MERCHANT_ID+"&orderId="+str(booking.id)+"_2"

        # for Production
        # url = "https://securegw.paytm.in/theia/api/v1/initiateTransaction?mid=YOUR_MID_HERE&orderId=ORDERID_98765"
        response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            
        txntoken = response["body"]["txnToken"]
        return render(request,"sheltownapp/paytmpayment.html",{'txnToken': txntoken,'mid': settings.MERCHANT_ID ,'orderId':str(booking.id)+"_2"})


    if request.method == "POST":
        form = PersonalReviewForm(request.POST)
        if form.is_valid():
            review = request.POST['review']
            rating = request.POST['rating']
            perser = request.POST['perser']
            print(perser)
            perser = get_object_or_404( PersonalService,name=perser)
            ob = PersonalServiceReview.objects.create(
                personalservice = perser,
                review = review,
                user = request.user.custom_user,
                rating = rating,

            )
            ob.save()
            messages.success(request,"Your review has been successfully registered")
            return HttpResponseRedirect(request.path_info)

    return render(request,'sheltownapp/useracctemp/user_service.html',context)    

@login_required
@normal_user_required
def bookingsuser(request):
    bookings = RentalBooking.objects.filter(booking_user=request.user.custom_user,first_payment=True,moved_out=False)
    rec_bookings = RentalRecurrentBooking.objects.filter(booking_user=request.user.custom_user,payment_complete=False)
    bookings_paid = RentalRecurrentBooking.objects.filter(booking_user=request.user.custom_user,payment_complete=True)
    bookings_movedout = RentalBooking.objects.filter(booking_user=request.user.custom_user,moved_out=True)
    if request.method == "GET" and "move_out_date" in request.GET:
        move_out_date = request.GET["move_out_date"]
        book_id = request.GET["book_id"]
        book = get_object_or_404(RentalBooking ,id=book_id)
        book.moved_out = True
        book.move_out_date = move_out_date
        book.save()
        for rec in rec_bookings:
            if rec.booked_property.name == book.booked_property.name:
                rec.moved_out = True
                rec.save()
        for rec in bookings_paid:
            if rec.booked_property.name == book.booked_property.name:
                rec.moved_out = True
                rec.save()
        return HttpResponse("Moved out sucessfully")
    if request.method == "GET" and "cancelid" in request.GET:
        id = request.GET['cancelid']
        rental = get_object_or_404(RentalBooking, id=id)
        rental.is_cancelled = True
        rental.save()
        return HttpResponse("Cancel Request Submitted")
    context ={
        'bookings' : bookings,
        'bookings_paid' : bookings_paid,
        'bookings_movedout' : bookings_movedout,
        'rec_bookings' : rec_bookings,
        'form' : RentalReviewForm(),
    }


    return render(request,"sheltownapp/useracctemp/booking_user.html", context)


def becomepartner(request):
    obj = BecomePartner.objects.first()
    context = {
        'obj' : obj
        
    }



    return render(request, 'sheltownapp/become-partner.html' ,context )

@login_required
@property_partner_required
def propertypartnerbooking(request):
    bookings = RentalBooking.objects.filter(property_partner=request.user.custom_user.property_owner,is_confirmed=True,moved_out=False)
    rec_bookings = RentalRecurrentBooking.objects.filter(property_partner=request.user.custom_user.property_owner,payment_complete=False)
    bookings_paid = RentalRecurrentBooking.objects.filter(property_partner=request.user.custom_user.property_owner,payment_complete=True)
    bookings_movedout = RentalBooking.objects.filter(property_partner=request.user.custom_user.property_owner,is_confirmed=True,moved_out=True)
    print(rec_bookings,bookings_paid)
    context ={
        'bookings' : bookings,
        'rec_bookings' : rec_bookings,
        'bookings_paid' : bookings_paid,
        'bookings_movedout' : bookings_movedout,
    }
    return render(request, 'sheltownapp/propacctemplate/booking_user.html',context)

def rentalreviewadd(request,pk):

    if request.method == "POST":
        form = PersonalReviewForm(request.POST)
        if form.is_valid():

            review = request.POST['review']
            rating = request.POST['rating']
            bookid = pk
            # print(perser)
            prop = get_object_or_404( RentalBooking,id = bookid).booked_property
            ob = PropertyReview.objects.create(
                property = prop,
                review = review,
                user = request.user.custom_user,
                rating = rating,

            )
            ob.save()
            messages.success(request,"Your review has been successfully registered")
    return HttpResponseRedirect(reverse('bookings-user'))

        